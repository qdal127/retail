# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* PZU - Web Application
* (PRIMA ZIRANG UTAMA)
* Version 1.0
* Author: 	Pudyasto Adi ( プディヤスト・アディ )
* Website: 	http://www.pudyastoadi.my.id/
* Contact: 	mr.pudyasto@gmail.com
* Follow: 	https://twitter.com/pudyastoadi
* Like: 	https://www.facebook.com/dhyaz.cs

### How do I get set up? ###

* Install database
* Copy all *.copy.php into general files *.php
      - config/config.copy.php
      - config/database.copy.php
      - config/ion_auth.copy.php
      - libraries/Apps.copy.php
* Customize your setting
      - config/config.php
      - config/database.php
      - config/ion_auth.php
      - libraries/Apps.php
* Try your application

### Contribution guidelines ###

* AdminLTE - https://almsaeedstudio.com/themes/AdminLTE/index2.html
* Codeigniter 3 - https://codeigniter.com/
* HMVC - https://bitbucket.org/wiredesignz/codeigniter-modular-extensions-hmvc
* Template Engine - https://github.com/philsturgeon/codeigniter-template
* Ion Auth - https://github.com/benedmunds/CodeIgniter-Ion-Auth

### Who do I talk to? ###

* mr.pudyasto@gmail.com


### IMPORTANT ###
A single license can be used only for one domain or client. 
Each use of the theme requires a separate license. 
We hope you will follow the rules as it will help us to continue supporting our themes and providing a free future updates.
Happy coding!
