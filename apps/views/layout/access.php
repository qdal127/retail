<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title><?php echo $template['title'];?></title>
    <!-- bootstrap 3.0.2 -->
    <link href="<?=base_url('assets/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet" type="text/css" />
    <!-- font Awesome -->
    <link href="<?php echo base_url('assets/bootstrap/css/font-awesome.min.css') ?>" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="<?php echo base_url('assets/bootstrap/css/ionicons.min.css') ?>" rel="stylesheet" type="text/css" />
    <!-- Morris chart -->
    <link href="<?php echo base_url('assets/bootstrap/css/morris/morris.css') ?>" rel="stylesheet" type="text/css" />
    <!-- jvectormap -->
    <link href="<?php echo base_url('assets/bootstrap/css/jvectormap/jquery-jvectormap-1.2.2.css') ?>" rel="stylesheet" type="text/css" />
    <!-- fullCalendar -->
    <link href="<?php echo base_url('assets/bootstrap/css/fullcalendar/fullcalendar.css') ?>" rel="stylesheet" type="text/css" />
    <!-- Daterange picker -->
    <link href="<?php echo base_url('assets/bootstrap/css/daterangepicker/daterangepicker-bs3.css') ?>" rel="stylesheet" type="text/css" />
    <!-- bootstrap wysihtml5 - text editor -->
    <link href="<?php echo base_url('assets/bootstrap/css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') ?>" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="<?php echo base_url('assets/bootstrap/css/AdminLTE.css') ?>" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .img-login{
            background-color: #f1ebeb;
            height: 250px;
            transform: rotate(-10deg);
            border: solid 5px #fff;
            box-shadow: 5px 5px 5px rgba(0, 0, 0, 0.3);
        }

        .login-logo, .register-logo {
            margin-bottom: 0px;
        }

        .login-box, .register-box {
            width: 320px;
            margin: 2% auto;
        }

        .login-page{
            background: linear-gradient(110deg, #d73925, #bd2714, #9d1909, #9d1909, #bd2714, #d73925);
            background-size: 1000% 1000%;

            -webkit-animation: AnimationName 15s ease infinite;
            -moz-animation: AnimationName 15s ease infinite;
            -o-animation: AnimationName 15s ease infinite;
            animation: AnimationName 15s ease infinite;

            height: auto;
        }

        .rainbow-login-page{
            background: linear-gradient(270deg, #662424, #665624, #4a6624, #276624, #246652, #245766, #243466, #3d2466, #542466, #66244c, #66242f, #662424);
            background-size: 2400% 2400%;

            -webkit-animation: Rainbow 30s ease infinite;
            -moz-animation: Rainbow 30s ease infinite;
            -o-animation: Rainbow 30s ease infinite;
            animation: Rainbow 30s ease infinite;
        }

        .login-box-body, .register-box-body{
            background: none;
        }

        .login-logo a, .register-logo a{
            color: #fff;
        }

        .login-box-msg, .register-box-msg {
            color: #fff;
        }

        @-webkit-keyframes AnimationName {
            0%{background-position:0% 50%}
            50%{background-position:100% 50%}
            100%{background-position:0% 50%}
        }
        @-moz-keyframes AnimationName {
            0%{background-position:0% 50%}
            50%{background-position:100% 50%}
            100%{background-position:0% 50%}
        }
        @-o-keyframes AnimationName {
            0%{background-position:0% 50%}
            50%{background-position:100% 50%}
            100%{background-position:0% 50%}
        }
        @keyframes AnimationName {
            0%{background-position:0% 50%}
            50%{background-position:100% 50%}
            100%{background-position:0% 50%}
        }

        @-webkit-keyframes Rainbow {
            0%{background-position:0% 50%}
            50%{background-position:100% 50%}
            100%{background-position:0% 50%}
        }
        @-moz-keyframes Rainbow {
            0%{background-position:0% 50%}
            50%{background-position:100% 50%}
            100%{background-position:0% 50%}
        }
        @-o-keyframes Rainbow {
            0%{background-position:0% 50%}
            50%{background-position:100% 50%}
            100%{background-position:0% 50%}
        }
        @keyframes Rainbow {
            0%{background-position:0% 50%}
            50%{background-position:100% 50%}
            100%{background-position:0% 50%}
        }
        @media (max-width: 768px){
            .img-side{
                display: none;
            }
        }
    </style>
</head>

<body class="hold-transition login-page">
    <?php echo $template['body'];?>

            <!-- jQuery 2.0.2 -->
            <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
            <!-- jQuery UI 1.10.3 -->
            <script src="<?php echo base_url('assets/bootstrap/js/jquery-ui-1.10.3.min.js') ?>" type="text/javascript"></script>
            <!-- Bootstrap -->
            <script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js') ?>" type="text/javascript"></script>
            <!-- Morris.js charts -->
            <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
            <script src="<?php echo base_url('assets/bootstrap/js/plugins/morris/morris.min.js') ?>" type="text/javascript"></script>
            <!-- Sparkline -->
            <script src="<?php echo base_url('assets/bootstrap/js/plugins/sparkline/jquery.sparkline.min.js') ?>" type="text/javascript"></script>
            <!-- jvectormap -->
            <script src="<?php echo base_url('assets/bootstrap/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') ?>" type="text/javascript"></script>
            <script src="<?php echo base_url('assets/bootstrap/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') ?>" type="text/javascript"></script>
            <!-- fullCalendar -->
            <script src="<?php echo base_url('assets/bootstrap/js/plugins/fullcalendar/fullcalendar.min.js') ?>" type="text/javascript"></script>
            <!-- jQuery Knob Chart -->
            <script src="<?php echo base_url('assets/bootstrap/js/plugins/jqueryKnob/jquery.knob.js') ?>" type="text/javascript"></script>
            <!-- daterangepicker -->
            <script src="<?php echo base_url('assets/bootstrap/js/plugins/daterangepicker/daterangepicker.js') ?>" type="text/javascript"></script>
            <!-- Bootstrap WYSIHTML5 -->
            <script src="<?php echo base_url('assets/bootstrap/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') ?>" type="text/javascript"></script>
            <!-- iCheck -->
            <script src="<?php echo base_url('assets/bootstrap/js/plugins/iCheck/icheck.min.js') ?>" type="text/javascript"></script>

            <!-- AdminLTE App -->
            <script src="<?php echo base_url('assets/bootstrap/js/AdminLTE/app.js') ?>" type="text/javascript"></script>

            <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
            <script src="<?php echo base_url('assets/bootstrap/js/AdminLTE/dashboard.js') ?>" type="text/javascript"></script>
    <script>
      $(function () {
          Pace.restart();
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });

        $(".alert").fadeOut(5000);
      });
    </script>
</body>

</html>
