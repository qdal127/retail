<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Profile
 *
 * @author adi
 */
class Profile extends MY_Controller {
    protected $data = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => 'Profil Pengguna',
            'msg_detail' => 'Ubah nama pengguna dan password di sini',
        
            'submit' => site_url('profile/submit'),
            'add' => site_url('profile/add'),
            'edit' => site_url('profile/edit'),
            'reload' => site_url('profile'),
        );
        $this->load->model('profile_qry');
    }

    //redirect if needed, otherwise display the user list
    
    public function index()
    {
        $this->data['user'] = $this->profile_qry->select_data();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }
    
    public function show_image(){
        $id = $this->session->userdata('username');
        $filename="";
        if(file_exists('./files/'.$id.".jpg")){
            $filename = './files/'.$id.".jpg";
        } elseif (file_exists('./files/'.$id.".jpeg")){
            $filename = './files/'.$id.".jpeg";
        } elseif (file_exists('./files/'.$id.".png")) {
            $filename = './files/'.$id.".png";
        }
        if (file_exists($filename)) {
            echo $this->apps->load_pict($filename);             
        }else{
            echo $this->apps->load_pict('./assets/dist/img/avatar.png');
        }
    }    
    
    public function submit() {
        $array = $this->input->post();
        if($this->_validate($array) == TRUE){
            $res = $this->profile_qry->submit();
            echo json_encode($res);
        }else{
            $res = array(
                'state' => 0,
                'msg' => strip_tags( validation_errors('','')),
            );
            echo json_encode($res);
        }
    }
    
    private function _validate($array) {
        $config = array(
            array(
                    'field' => 'kduser',
                    'label' => 'Nama Pengguna',
                    'rules' => 'max_length[50]|min_length[2]|required|alpha_numeric',
                    'errors' => array(
                            'alpha_numeric' => 'Nama Pengguna hanya berisi huruf dan angka saja!',
                            ),
                    ),
            array(
                    'field' => 'nmuser',
                    'label' => 'Nama Lengkap Pengguna',
                    'rules' => 'max_length[100]|min_length[2]|required',
                    ),
            array(
                    'field' => 'password1',
                    'label' => 'Password Baru',
                    'rules' => 'max_length[50]|min_length[2]',
                    ),
            array(
                    'field' => 'password2',
                    'label' => 'Konfirmasi Password Baru',
                    'rules' => 'max_length[50]|min_length[2]|matches[password1]',
                    'errors' => array(
                            'matches' => 'Password tidak sama dengan password awal',
                            ),
                    ),
        );
        
        $this->form_validation->set_rules($config);   
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
}
