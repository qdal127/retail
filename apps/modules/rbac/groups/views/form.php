<?php

/* 
 * ***************************************************************
 * Script : 
 * Version : 
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description : 
 * ***************************************************************
 */

?>   
<div class="row">
		<!-- left column -->
		<div class="col-md-12">
			<!-- general form elements -->
			<div class="box box-danger">
				<!-- 
				<div class="box-header with-border">
					<h3 class="box-title">{msg_main}</h3>
				</div>
				-->
				<!-- /.box-header -->
				<!-- form start -->
				<?php
						$attributes = array(
								'role=' => 'form'
								, 'id' => 'form_add'
								, 'name' => 'form_add');
						echo form_open($submit,$attributes); 
				?> 
					<div class="box-body">

						<div class="form-group">
								<?php 
										echo form_input($form['id']);

										echo form_label($form['name']['placeholder']);
										echo form_input($form['name']);
										echo form_error('nmgroups','<div class="note">','</div>'); 
								?>
						</div>

						<div class="form-group">
								<?php 
										echo form_label($form['description']['placeholder']);
										echo form_textarea($form['description']);
										echo form_error('description','<div class="note">','</div>'); 
								?>
						</div>

						<div class="form-group">
								<?php 
										echo form_label($form['dashboard']['placeholder']);
										echo form_input($form['dashboard']);
										echo form_error('dashboard','<div class="note">','</div>'); 
								?>
						</div>

						<div class="form-group">
							<div class="checkbox">
								<label>
									<?=form_checkbox($form['faktif']) . $form['faktif']['placeholder'];?>
								</label>
							</div>
							<?=form_error('faktif','<div class="note">','</div>');?>
						</div>

					</div>
					<!-- /.box-body -->

					<div class="box-footer">
						<button type="submit" class="btn btn-primary" data-toggle="tooltip" data-placement="auto" title="Simpan Data"><span class="fa fa-check"></span> 
							Simpan
						</button>
						<a href="<?php echo $reload;?>" class="btn btn-danger" data-toggle="tooltip" data-placement="auto" title="Batal Simpan"><span class="fa fa-remove"></span> 
							Batal
						</a>
					</div>
				<?php echo form_close(); ?>
			</div>
			<!-- /.box -->
		</div>
</div>

<script type="text/javascript">
		$(document).ready(function () {
				
		});
</script>