<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of M_prod
 *
 * @author adi
 */
class M_prod extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,

            'submit' => site_url('m_prod/submit'),
            'add' => site_url('m_prod/add'),
            'edit' => site_url('m_prod/edit'),
            'reload' => site_url('m_prod'),
        );
        $this->load->model('m_prod_qry'); 

        $this->data['inv'] = array(
            "INVENTORY" => "INVENTORY",
            "NON-INVENTORY" => "NON-INVENTORY"
        ); 

        $kategori = $this->m_prod_qry->getKategori();
        foreach ($kategori as $value) {
            $this->data['kdktgprod'][$value['kdktgprod']] = $value['nmktgprod'];
        }
        $satuan = $this->m_prod_qry->getSatuan();
        foreach ($satuan as $value) {
            $this->data['kdstnprod'][$value['kdstnprod']] = $value['nmstnprod'];
        }

    }

    //redirect if needed, otherwise display the user list

    public function index(){

        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }

    public function add(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    }

    public function edit() {
        $this->_init_edit();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    }

    public function json_dgview() {
        echo $this->m_prod_qry->json_dgview();
    }
/*
    public function getKategori() {
        echo $this->m_prod_qry->getKategori();
    }*/ 

    public function submit() {  
        $stat = $this->input->post('stat');        $kdprod = $this->input->post('kdprod'); 
        if($this->validate($kdprod,$stat) == TRUE){
            $res = $this->m_prod_qry->submit();
            if(empty($stat)){
                $data = json_decode($res);
                if($data->state==="0"){
                    if(empty($kdprod)){
                        $this->_init_add();
                        $this->template->build('form', $this->data);
                    }else{
                        $this->_check_id($kdprod);
                        $this->template->build('form', $this->data);
                    }
                }else{
                    redirect($this->data['reload']);
                }
            }else{
                echo $res;
            }
        }else{
            if(empty($kdprod)){
                $this->_init_add();
                $this->template->build('form', $this->data);
            }else{
                $this->_init_edit();
                $this->_check_id($kdprod);
                $this->template->build('form', $this->data);
            }
        }
    } 

    private function _init_add(){

        if(isset($_POST['fstatus']) && strtoupper($_POST['fstatus']) == 't'){
          $faktif = TRUE;
        } else{
          $faktif = FALSE;
        }

        $this->data['form'] = array(
           'kdprod'=> array(
                     'type'        => 'hidden',
                    'placeholder' => 'Kode Produk',
                    'id'          => 'kdprod',
                    'name'        => 'kdprod',
                    'value'       => set_value('kdprod'),
                    'class'       => 'form-control',
                    // 'readonly'    => '',
                    'required'    => '',
            ),
           'nmprod'=> array(
                    'placeholder' => 'Nama Produk',
                    'id'      => 'nmprod',
                    'name'        => 'nmprod',
                    'value'       => set_value('nmprod'),
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
            ), 
           'sku'=> array(
                    'placeholder' => 'SKU/Barcode',
                    'id'      => 'sku',
                    'name'        => 'sku',
                    'value'       => set_value('sku'),
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
            ), 
           'tipeprod'=> array(
                    'attr'        => array(
                        'id'    => 'tipeprod',
                        'class' => 'form-control chosen-select',
                    ),
                        'name'    => 'tipeprod',
                    'placeholder' => 'Tipe Produk',  
                    'data'        => $this->data['inv'],
                    'value'       => set_value('tipeprod'), 
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
            ), 
            'kdktgprod'=> array(
                    'attr'        => array(
                        'id'    => 'kdktgprod',
                        'class' => 'form-control chosen-select',
                    ),
                    'data'     =>  $this->data['kdktgprod'],
                    'value'    => set_value('kdktgprod'),
                    'name'     => 'kdktgprod',
                    'required'    => '',
                    'placeholder' => 'Kategori',
            ), 
           'kdstnprod'=> array(
                    'placeholder' => 'Satuan',
                    'attr'        => array(
                        'id'    => 'kdstnprod', 
                        'class' => 'form-control chosen-select',
                    ),
                    'name'    => 'kdstnprod',
                    'data'        => $this->data['kdstnprod'],
                    'value'       => set_value('kdstnprod'), 
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
            ),  
           'notes'=> array(
                    'placeholder' => 'Notes',
                    'id'      => 'notes',
                    'name'        => 'notes',
                    'value'       => set_value('notes'),
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
            ), 
      		'fstatus'=> array(
                    'placeholder' => '',
      				'id'          => 'fstatus',
      				'value'       => 't',
      				'checked'     => $faktif,
      				'class'       => 'custom-control-input',
      				'name'		  => 'fstatus',
      				'type'		  => 'checkbox',
      			),
        );
    }

    private function _init_edit($no = null){

        if(!$no){
            $kdprod = $this->uri->segment(3);
        }
        $this->_check_id($kdprod);

        if($this->val[0]['fstatus'] == 't'){
        			$faktifx = true;
        		} else {
            			$faktifx = false;
            }
        $this->data['form'] = array( 
           'kdprod'=> array(
                     'type'        => 'hidden',
                    'placeholder' => 'Kode Produk',
                    'id'          => 'kdprod',
                    'name'        => 'kdprod',
                    'value'       => $this->val[0]['kdprod'],
                    'class'       => 'form-control',
                    // 'readonly'    => '',
                    'required'    => '',
            ),
           'nmprod'=> array(
                    'placeholder' => 'Nama Produk',
                    'id'      => 'nmprod',
                    'name'        => 'nmprod',
                    'value'       => $this->val[0]['nmprod'],
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
            ), 
           'sku'=> array(
                    'placeholder' => 'SKU/Barcode',
                    'id'      => 'sku',
                    'name'        => 'sku',
                    'value'       => $this->val[0]['sku'],
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
            ), 
           'tipeprod'=> array(
                    'attr'        => array(
                        'id'    => 'tipeprod',
                        'class' => 'form-control chosen-select',
                    ),
                        'name'    => 'tipeprod',
                    'placeholder' => 'Tipe Produk',  
                    'data'        => $this->data['inv'],
                    'value'       => $this->val[0]['tipeprod'], 
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
            ), 
           'kdktgprod'=> array(
                    'placeholder' => 'Kategori',
                    'attr'        => array(
                        'id'    => 'kdktgprod',
                        'class' => 'form-control chosen-select',
                    ),
                        'name'    => 'kdktgprod',
                    'data'        => $this->data['kdktgprod'],
                    'value'       => $this->val[0]['kdktgprod'], 
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
            ), 
           'kdstnprod'=> array(
                    'placeholder' => 'Satuan',
                    'attr'        => array(
                        'id'    => 'kdstnprod',
                        'class' => 'form-control chosen-select',
                    ),
                        'name'    => 'kdstnprod',
                    'data'        => $this->data['kdstnprod'],
                    'value'       => $this->val[0]['kdstnprod'], 
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
            ),  
           'notes'=> array(
                    'placeholder' => 'Notes',
                    'id'      => 'notes',
                    'name'        => 'notes',
                    'value'       => $this->val[0]['notes'],
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
            ), 
      		'fstatus'=> array(
                    'placeholder' => 'Status Item',
      				'id'          => 'fstatus',
      				'value'       => $faktifx,
      				'checked'     => $faktifx,
      				'class'       => 'custom-control-input',
      				'name'		  => 'fstatus',
      				'type'		  => 'checkbox',
      			),
        );
    }

    private function _check_id($kdprod){
        if(empty($kdprod)){
            redirect($this->data['add']);
        }

        $this->val= $this->m_prod_qry->select_data($kdprod);

        if(empty($this->val)){
            redirect($this->data['add']);
        }
    }

    private function validate($kdprod,$stat) {
        if(!empty($stat)){
            return true;
        }
        $config = array( 
            array(
                    'field' => 'nmprod',
                    'label' => 'Nama Produk',
                    'rules' => 'required|max_length[20]',
                ),
            array(
                    'field' => 'sku',
                    'label' => 'SKU',
                    'rules' => 'required|max_length[255]',
                ),
            array(
                    'field' => 'tipeprod',
                    'label' => 'Tipe',
                    'rules' => 'required|max_length[20]',
                ),
            array(
                    'field' => 'kdktgprod',
                    'label' => 'Kode Kategori',
                    'rules' => 'required|max_length[255]',
                ),
            array(
                    'field' => 'kdstnprod',
                    'label' => 'Kode Satuan',
                    'rules' => 'required|max_length[20]',
                ), 
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
}
