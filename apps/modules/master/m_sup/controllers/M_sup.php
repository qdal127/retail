<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of M_sup
 *
 * @author adi
 */
class M_sup extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,

            'submit' => site_url('m_sup/submit'),
            'add' => site_url('m_sup/add'),
            'edit' => site_url('m_sup/edit'),
            'reload' => site_url('m_sup'),
        );
        $this->load->model('m_sup_qry'); 

    }

    //redirect if needed, otherwise display the user list

    public function index(){

        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }

    public function add(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    }

    public function edit() {
        $this->_init_edit();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    }

    public function json_dgview() {
        echo $this->m_sup_qry->json_dgview();
    }
/*
    public function getKategori() {
        echo $this->m_sup_qry->getKategori();
    }*/ 

    public function submit() {  
        $stat = $this->input->post('stat');        $kdsup = $this->input->post('kdsup'); 
        if($this->validate($kdsup,$stat) == TRUE){
            $res = $this->m_sup_qry->submit();
            if(empty($stat)){
                $data = json_decode($res);
                if($data->state==="0"){
                    if(empty($kdsup)){
                        $this->_init_add();
                        $this->template->build('form', $this->data);
                    }else{
                        $this->_check_id($kdsup);
                        $this->template->build('form', $this->data);
                    }
                }else{
                    redirect($this->data['reload']);
                }
            }else{
                echo $res;
            }
        }else{
            if(empty($kdsup)){
                $this->_init_add();
                $this->template->build('form', $this->data);
            }else{
                $this->_init_edit();
                $this->_check_id($kdsup);
                $this->template->build('form', $this->data);
            }
        }
    } 

    private function _init_add(){

        if(isset($_POST['fstatus']) && strtoupper($_POST['fstatus']) == 't'){
          $faktif = TRUE;
        } else{
          $faktif = FALSE;
        }

        $this->data['form'] = array(
           'kdsup'=> array(
                     'type'        => 'hidden',
                    'placeholder' => 'Kode Supplies',
                    'id'          => 'kdsup',
                    'name'        => 'kdsup',
                    'value'       => set_value('kdsup'),
                    'class'       => 'form-control',
                    // 'readonly'    => '',
                    'required'    => '',
            ),
           'nmsup'=> array(
                    'placeholder' => 'Nama Supplies',
                    'id'      => 'nmsup',
                    'name'        => 'nmsup',
                    'value'       => set_value('nmsup'),
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
            ), 
           'pic'=> array(
                    'placeholder' => 'PIC',
                    'id'      => 'pic',
                    'name'        => 'pic',
                    'value'       => set_value('pic'),
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
            ), 
           'nohp'=> array(
                    'placeholder' => 'Nomor HP',
                    'id'      => 'nohp',
                    'name'        => 'nohp',
                    'value'       => set_value('nohp'),
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
            ), 
           'alamat'=> array(
                    'placeholder' => 'Alamat Supplies',
                    'id'      => 'alamat',
                    'name'        => 'alamat',
                    'value'       => set_value('alamat'),
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
            ), 
           'kota'=> array(
                    'placeholder' => 'Kota',
                    'id'      => 'kota',
                    'name'        => 'kota',
                    'value'       => set_value('kota'),
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
            ), 
           'npwp'=> array(
                    'placeholder' => 'NPWP',
                    'id'      => 'npwp',
                    'name'        => 'npwp',
                    'value'       => set_value('npwp'),
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
            ), 
           'noktp'=> array(
                    'placeholder' => 'No. KTP',
                    'id'      => 'noktp',
                    'name'        => 'noktp',
                    'value'       => set_value('noktp'),
                    'class'       => 'form-control',
                    // 'required'    => '',
                    'style'       => 'text-transform: uppercase;',
            ), 
           'notes'=> array(
                    'placeholder' => 'Notes',
                    'id'      => 'notes',
                    'name'        => 'notes',
                    'value'       => set_value('notes'),
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
            ), 
      		'fstatus'=> array(
                    'placeholder' => '',
      				'id'          => 'fstatus',
      				'value'       => 't',
      				'checked'     => $faktif,
      				'class'       => 'custom-control-input',
      				'name'		  => 'fstatus',
      				'type'		  => 'checkbox',
      			),
        );
    }

    private function _init_edit($no = null){

        if(!$no){
            $kdsup = $this->uri->segment(3);
        }
        $this->_check_id($kdsup);

        if($this->val[0]['fstatus'] == 't'){
        			$faktifx = true;
        		} else {
            			$faktifx = false;
            }
        $this->data['form'] = array( 
           'kdsup'=> array(
                     'type'        => 'hidden',
                    'placeholder' => 'Kode Supplies',
                    'id'          => 'kdsup',
                    'name'        => 'kdsup',
                    'value'       => $this->val[0]['kdsup'],
                    'class'       => 'form-control',
                    // 'readonly'    => '',
                    'required'    => '',
            ),
           'nmsup'=> array(
                    'placeholder' => 'Nama Supplies',
                    'id'      => 'nmsup',
                    'name'        => 'nmsup',
                    'value'       => $this->val[0]['nmsup'],
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
            ), 
           'pic'=> array(
                    'placeholder' => 'PIC',
                    'id'      => 'pic',
                    'name'        => 'pic',
                    'value'       => $this->val[0]['pic'],
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
            ), 
           'nohp'=> array(
                    'placeholder' => 'Nomor HP',
                    'id'      => 'nohp',
                    'name'        => 'nohp',
                    'value'       => $this->val[0]['nohp'],
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
            ), 
           'alamat'=> array(
                    'placeholder' => 'Alamat Supplies',
                    'id'      => 'alamat',
                    'name'        => 'alamat',
                    'value'       => $this->val[0]['alamat'],
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
            ), 
           'kota'=> array(
                    'placeholder' => 'Kota',
                    'id'      => 'kota',
                    'name'        => 'kota',
                    'value'       => $this->val[0]['kota'],
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
            ), 
           'npwp'=> array(
                    'placeholder' => 'NPWP',
                    'id'      => 'npwp',
                    'name'        => 'npwp',
                    'value'       => $this->val[0]['npwp'],
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
            ), 
           'noktp'=> array(
                    'placeholder' => 'No. KTP',
                    'id'      => 'noktp',
                    'name'        => 'noktp',
                    'value'       => $this->val[0]['noktp'],
                    'class'       => 'form-control',
                    // 'required'    => '',
                    'style'       => 'text-transform: uppercase;',
            ), 
           'notes'=> array(
                    'placeholder' => 'Notes',
                    'id'      => 'notes',
                    'name'        => 'notes',
                    'value'       => $this->val[0]['notes'],
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
            ), 
      		'fstatus'=> array(
                    'placeholder' => 'Status Item',
      				'id'          => 'fstatus',
      				'value'       => $faktifx,
      				'checked'     => $faktifx,
      				'class'       => 'custom-control-input',
      				'name'		  => 'fstatus',
      				'type'		  => 'checkbox',
      			),
        );
    }

    private function _check_id($kdsup){
        if(empty($kdsup)){
            redirect($this->data['add']);
        }

        $this->val= $this->m_sup_qry->select_data($kdsup);

        if(empty($this->val)){
            redirect($this->data['add']);
        }
    }

    private function validate($kdsup,$stat) {
        if(!empty($stat)){
            return true;
        }
        $config = array( 
            array(
                    'field' => 'nmsup',
                    'label' => 'Nama Supplies',
                    'rules' => 'required|max_length[20]',
                ),
            array(
                    'field' => 'pic',
                    'label' => 'PIC',
                    'rules' => 'required|max_length[255]',
                ),
            array(
                    'field' => 'nohp',
                    'label' => 'No. HP',
                    'rules' => 'required|max_length[20]',
                ),
            array(
                    'field' => 'alamat',
                    'label' => 'Alamat Supplies',
                    'rules' => 'required|max_length[255]',
                ),
            array(
                    'field' => 'kota',
                    'label' => 'Kota Supplies',
                    'rules' => 'required|max_length[20]',
                ),
            array(
                    'field' => 'npwp',
                    'label' => 'NPWP Supplies',
                    'rules' => 'required|max_length[20]',
                ), 
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
}
