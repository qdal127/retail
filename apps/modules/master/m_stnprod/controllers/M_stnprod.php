<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of M_stnprod
 *
 * @author adi
 */
class M_stnprod extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,

            'submit' => site_url('m_stnprod/submit'),
            'add' => site_url('m_stnprod/add'),
            'edit' => site_url('m_stnprod/edit'),
            'reload' => site_url('m_stnprod'),
        );
        $this->load->model('m_stnprod_qry'); 

    }

    //redirect if needed, otherwise display the user list

    public function index(){

        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }

    public function add(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    }

    public function edit() {
        $this->_init_edit();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    }

    public function json_dgview() {
        echo $this->m_stnprod_qry->json_dgview();
    }
/*
    public function getSatuan() {
        echo $this->m_stnprod_qry->getSatuan();
    }*/ 

    public function submit() {  
        $kdstnprod = $this->input->post('kdstnprod'); 
        $stat = $this->input->post('stat');
        if($this->validate($kdstnprod,$stat) == TRUE){
            $res = $this->m_stnprod_qry->submit();
            if(empty($stat)){
                $data = json_decode($res);
                if($data->state==="0"){
                    if(empty($kdstnprod)){
                        $this->_init_add();
                        $this->template->build('form', $this->data);
                    }else{
                        $this->_check_id($kdstnprod);
                        $this->template->build('form', $this->data);
                    }
                }else{
                    redirect($this->data['reload']);
                }
            }else{
                echo $res;
            }
        }else{
            if(empty($kdstnprod)){
                $this->_init_add();
                $this->template->build('form', $this->data);
            }else{
                $this->_init_edit();
                $this->_check_id($kdstnprod);
                $this->template->build('form', $this->data);
            }
        }
    } 

    private function _init_add(){

        if(isset($_POST['fstatus']) && strtoupper($_POST['fstatus']) == 't'){
          $faktif = TRUE;
        } else{
          $faktif = FALSE;
        }

        $this->data['form'] = array(
           'kdstnprod'=> array(
                     'type'        => 'hidden',
                    'placeholder' => 'Kode Satuan Produk',
                    'id'          => 'kdstnprod',
                    'name'        => 'kdstnprod',
                    'value'       => set_value('kdstnprod'),
                    'class'       => 'form-control',
                    // 'readonly'    => '',
                    'required'    => '',
            ),
           'nmstnprod'=> array(
                    'placeholder' => 'Nama Satuan Produk',
                    'id'      => 'nmstnprod',
                    'name'        => 'nmstnprod',
                    'value'       => set_value('nmstnprod'),
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;',
            ), 
      		'fstatus'=> array(
                    'placeholder' => '',
      				'id'          => 'fstatus',
      				'value'       => 't',
      				'checked'     => $faktif,
      				'class'       => 'custom-control-input',
      				'name'		  => 'fstatus',
      				'type'		  => 'checkbox',
      			),
        );
    }

    private function _init_edit($no = null){

        if(!$no){
            $kdstnprod = $this->uri->segment(3);
        }
        $this->_check_id($kdstnprod);

        if($this->val[0]['fstatus'] == 't'){
        			$faktifx = true;
        		} else {
            			$faktifx = false;
            }
        $this->data['form'] = array(
           'kdstnprod'=> array(
                    'type'        => 'hidden',
                    'placeholder' => 'Kode Satuan Produk',
                    'id'          => 'kdstnprod',
                    'name'        => 'kdstnprod',
                    'value'       => $this->val[0]['kdstnprod'],
                    'class'       => 'form-control',
                    'readonly'    => '',
            ),
           'nmstnprod'=> array(
                    'placeholder' => 'Nama Satuan Produk',
                    'id'          => 'nmstnprod',
                    'name'        => 'nmstnprod',
                    'value'       => $this->val[0]['nmstnprod'],
                    'class'       => 'form-control',
                    'required'    => '',
                    'style'       => 'text-transform: uppercase;'
            ), 
      		'fstatus'=> array(
                    'placeholder' => 'Status Item',
      				'id'          => 'fstatus',
      				'value'       => $faktifx,
      				'checked'     => $faktifx,
      				'class'       => 'custom-control-input',
      				'name'		  => 'fstatus',
      				'type'		  => 'checkbox',
      			),
        );
    }

    private function _check_id($kdstnprod){
        if(empty($kdstnprod)){
            redirect($this->data['add']);
        }

        $this->val= $this->m_stnprod_qry->select_data($kdstnprod);

        if(empty($this->val)){
            redirect($this->data['add']);
        }
    }

    private function validate($kdstnprod,$stat) {
        if(!empty($stat)){
            return true;
        }
        $config = array( 
            array(
                    'field' => 'nmstnprod',
                    'label' => 'Nama Satuan Produk',
                    'rules' => 'required|max_length[20]',
                ),
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
}
