<?php

/*
 * ***************************************************************
 * Script :
 * Version :
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description :
 * ***************************************************************
 */
?>
<style>
    #myform .errors {
    color: red;
    }

    #modalform .errors {
    color: red;
    }
</style>
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
        <!-- general form elements -->
        <form id="myform" name="myform" method="post">
            <div class="box box-danger main-form">
                <!-- .box-header -->
                <!--
                <div class="box-header with-border">
                    <h3 class="box-title">{msg_main}</h3>
                </div>
                -->
                <!-- /.box-header -->

                <!-- form start -->
                <?php
                    $attributes = array(
                        'role=' => 'form'
                      , 'id' => 'form_add'
                      , 'name' => 'form_add'
                      , 'enctype' => 'multipart/form-data'
                      , 'data-validate' => 'parsley');
                    echo form_open($submit,$attributes);
                ?>
                <!-- /form start -->

                <!-- .box-body -->
                <div class="box-body">
                    <div class="row">

                        <div class="col-md-12 col-lg-12">
                            <div class="row">
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <?php
                                            echo form_label($form['noitem_out']['placeholder']);
                                            echo form_input($form['noitem_out']);
                                            echo form_error('noitem_out','<div class="note">','</div>');
                                        ?>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <?php
                                            echo form_label($form['tglitem_out']['placeholder']);
                                            echo form_input($form['tglitem_out']);
                                            echo form_error('tglitem_out','<div class="note">','</div>');
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div> 

                        <div class="col-md-12 col-lg-12">
                            <div class="row">
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <?php
                                            echo form_label($form['ket']['placeholder']);
                                            echo form_input($form['ket']);
                                            echo form_error('ket','<div class="note">','</div>');
                                        ?>
                                    </div>
                                </div> 
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <?php 
                                            echo form_input($form['nourut']);
                                            echo form_error('nourut','<div class="note">','</div>');
                                        ?>
                                    </div>
                                </div> 
                            </div>
                        </div> 

                        <div class="col-md-12">
                            <div style="border-top: 1px solid #ddd; height: 10px;"></div>
                        </div> 

                        <div class="col-md-6 col-lg-6">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <button type="button" class="btn btn-primary btn-add">Tambah</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-hover dataTable">
                                    <thead>
                                        <tr>
                                            <th style="width: 100px;text-align: center;">Nama Barang</th>
                                            <th style="width: 100px;text-align: center;">Quantity</th>
                                            <th style="width: 100px;text-align: center;">harga</th>
                                            <th style="width: 100px;text-align: center;">total</th>
                                            <th style="width: 10px;text-align: center;">Edit</th>
                                            <th style="width: 10px;text-align: center;">Hapus</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                    <tfoot>
                                        <tr>
                                            <th></th>
                                            <th style="text-align: center;"></th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-12">
                            <div class="row">
                                <div class="col-xs-4">
                                    <div class="form-group">
                                        <?php
                                            echo form_label($form['tot1']['placeholder']);
                                            echo form_input($form['tot1']);
                                            echo form_error('tot1','<div class="note">','</div>');
                                        ?>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?php
                                            echo '<b>Potongan</b>';
                                            echo form_input($form['pot']);
                                            echo form_error('pot','<div class="note">','</div>');
                                        ?>
                                    </div>
                                </div> 
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?php
                                            echo form_label($form['byongkos']['placeholder']);
                                            echo form_input($form['byongkos']);
                                            echo form_error('byongkos','<div class="note">','</div>');
                                        ?>
                                    </div>
                                </div> 
                            </div>
                        </div>


                        <div class="col-md-12 col-lg-12">
                            <div class="row"> 

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?php
                                            echo form_label($form['tot2']['placeholder']);
                                            echo form_input($form['tot2']);
                                            echo form_error('tot2','<div class="note">','</div>');
                                        ?>
                                    </div>
                                </div> 

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?php
                                            echo form_label($form['byr']['placeholder']);
                                            echo form_input($form['byr']);
                                            echo form_error('byr','<div class="note">','</div>');
                                        ?>
                                    </div>
                                </div> 

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?php
                                            echo form_label($form['tot_akhir']['placeholder']);
                                            echo form_input($form['tot_akhir']);
                                            echo form_error('tot_akhir','<div class="note">','</div>');
                                        ?>
                                    </div>
                                </div> 
                                
                            </div>
                        </div>   
                    </div>
                </div>
                <!-- /.box-body -->

                <!-- .box-footer -->
                <div class="box-footer">
                    <button type="button" class="btn btn-primary btn-submit">
                        Simpan
                    </button>
                    <button type="button" class="btn btn-default btn-batal">
                        Batal
                    </button>
                </div>
                <!-- /.box-footer -->
            </div>
        </form>
        <!-- /.box -->
    </div>
</div>
<!-- modal dialog -->
<div id="modal_transaksi" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="modalform" name="modalform" method="post">
                <!-- .modal-header -->
                <div class="modal-header">
                    <h4 class="modal-title">Form Detail Transaksi</h4>
                </div>

                <!-- .modal-body -->
                <div class="modal-body">

                    <div class="col-xs-12">
                        <div class="row">
                            <div class="form-group">
                                <?php
                                    echo form_label('Pilih Nama Barang');
                                    echo form_dropdown( $form['kditem']['name'],
                                                        $form['kditem']['data'] ,
                                                        $form['kditem']['value'] ,
                                                        $form['kditem']['attr']);
                                    echo form_error('kditem','<div class="note">','</div>');
                                ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div class="row">
                            <div class="form-group">
                                <?php
                                    echo form_label($form['qty']['placeholder']);
                                    echo form_input($form['qty']);
                                    echo form_error('qty','<div class="note">','</div>');
                                ?>
                            </div>
                          </div>
                      </div>

                    <div class="col-xs-12">
                        <div class="row">
                            <div class="form-group">
                                <?php
                                    echo form_label($form['harga']['placeholder']);
                                    echo form_input($form['harga']);
                                    echo form_error('harga','<div class="note">','</div>');
                                ?>
                            </div>
                          </div>
                      </div>

                    <!-- .modal-footer -->
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn btn-outline-danger btn-elevate btn-cancel">Batal</button>
                        <button type="button" class="btn btn-success btn-elevate btn-simpan">Simpan</button>
                    </div>
                    <!-- /.modal-footer -->
                </div>
                <!-- /.modal-body -->
            </form>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>

<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/jquery.validate.js" ></script>

<script type="text/javascript">
    $(document).ready(function () {  
        getKodeitemout(); 

        var validator = $('#modalform').validate({
            errorClass: 'errors',
            rules : {
                kdakun  : "required",
                nominal : "required"
            },
            messages : {
                kdakun  : "Masukkan Kode Akun",
                nominal : "Masukkan Nominal"
            },
            highlight: function (element) {
                $(element).parent().addClass('errors')
            },
            unhighlight: function (element) {
                $(element).parent().removeClass('errors')
            }
        });
        $('#qty').autoNumeric('init', {aSep: '.', aDec: ',', mDec: '0'});
        $('#tot1').autoNumeric('init');
        $('#pot').autoNumeric('init');
        $('#byongkos').autoNumeric('init');
        $('#tot2').autoNumeric('init');
        $('#byr').autoNumeric('init');
        $('#tot_akhir').autoNumeric('init');
        $('#harga').autoNumeric('init'); 

        var column = [];

        //column.push({ "aDataSort": [ 1,0 ], "aTargets": [ 1 ] });
        //column.push({ "aDataSort": [ 0,1,2,3,4 ], "aTargets": [ 4 ] });

        column.push({
            "aTargets": [ 2,3 ],
            "mRender": function (data, type, full) {
                return type === 'export' ? data : numeral(data).format('0,0.00');
            },
            "sClass": "right"
        });

        column.push({
            "aTargets": [ 4,5 ],
            "sClass": "center"
        });

        column.push({
            "aTargets": [ 1 ],
            "mRender": function (data, type, full) {
                return type === 'export' ? data : numeral(data).format('0,0');
            },
            "sClass": "right"
        });


        column.push({
            "aTargets": [ 1,2,3 ],
            "sClass": "center"
        });

        table = $('.dataTable').DataTable({
            "aoColumnDefs": column,
            "columns": [
                { "data": "nmitem"  },
                { "data": "qty" },
                { "data": "harga" },
                { "data": "total" },
                { "data": "edit" },
                { "data": "delete"}
            ],
            "lengthMenu": [[ -1], [ "Semua Data"]],
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            //"ordering": false,
            "bSort": false,
            "bAutoWidth": false,
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push({ "name": "noitem_out", "value": $("#noitem_out").val()});
                $.ajax( {
                    "dataType": 'json',
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": fnCallback
                } );
            },
            'rowCallback': function(row, data, index){
            },
            "sAjaxSource": "<?=site_url('itemout/json_dgview_detail');?>",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
            },
            // jumlah TOTAL
            'footerCallback': function ( row, data, start, end, display ) {
                var api = this.api(), data;

                // converting to interger to find total
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };

                // computing column Total of the complete result

                var total = api
                    .column( 3 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                // Update footer by showing the total with the reference of the column index
                $( api.column( 2 ).footer() ).html('Total');
                $( api.column( 3 ).footer() ).html(numeral(total).format('0,0.00'));
                $('#tot1').autoNumeric('set',total); 
                sum();
            },
            buttons: [{
                extend:    'excelHtml5', footer: true,
                text:      'Export To Excel',
                titleAttr: 'Excel',
                "oSelectorOpts": { filter: 'applied', order: 'current' },
                "sFileName": "report.xls",
                action : function( e, dt, button, config ) {
                    exportTableToCSV.apply(this, [$('.dataTable'), 'export.xls']);
                },
                exportOptions: {orthogonal: 'export'}
            }],
            "sDom": "<'row'<'col-sm-6'><'col-sm-6 text-right'> r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
        }); 


        $('.btn-add').click(function(){
            validator.resetForm();
          	$('#modal_transaksi').modal('toggle');
            $('#kditem').select2({
                placeholder: '-- Pilih Nama Item/Obat --',
                dropdownAutoWidth : true,
                width: '100%',
                escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
              //  templateResult: formatSalesHeader, // omitted for brevity, see the source of this page
              //  templateSelection: formatSalesHeaderSelection // omitted for brevity, see the source of this page
            });
            //alert($('nodo').val());
            clear();
        }); 

        $('#kditem').change(function(){
            set_harga();
        });

        $('.btn-simpan').click(function(){
            if ($("#modalform").valid()) {
                addDetail();
            }
        });

        $('.btn-update').click(function(){
            if ($("#modalform").valid()) {
                UpdateDetail();
            }
        });

        $('.btn-cancel').click(function(){
            validator.resetForm();
            $("#nourut").val('');
            clear();
        });

        $(".btn-batal").click(function(){
            batal();
            clear();
        });

        $(".btn-submit").click(function(){
            if ($("#myform").valid()) {
              submit();
            }
        });
    });   

    function set_harga(){
        var kditem = $('#kditem').val();
        $.ajax({
            type:"POST",
            url:"<?=site_url("itemout/set_harga");?>",
            data:{"kditem":kditem},
            success: function(resp){
                var obj = jQuery.parseJSON(resp);
                $.each(obj,function(key, value){
                    $('#harga').autoNumeric('set',value.harga);
                });
            }
        });
    }

    function getKodeitemout(){
       //alert(kddiv);
        $.ajax({
            type: "POST",
            url: "<?=site_url("itemout/getKodeitemout");?>",
            data: {},
            beforeSend: function() {
                $('#kditem').html("")
                            .append($('<option>', { value : '' })
                            .text('-- Pilih Nama Barang --'));
                $("#kditem").trigger("change.chosen");
                if ($('#kditem').hasClass("chosen-hidden-accessible")) {
                    $('#kditem').select2('destroy');
                    $("#kditem").chosen({ width: '100%' });
                }
            },
            success: function(resp){
                var obj = jQuery.parseJSON(resp);
                $.each(obj, function(key, value){
                    $('#kditem')
                        .append($('<option>', { value : value.kditem })
                        .html("<b style='font-size: 14px;'>" + value.nmitem + " </b>"));
                });

                function formatSalesHeader (repo) {
                    if (repo.loading) return "Mencari data ... ";
                    var separatora = repo.text.indexOf("[");
                    var separatorb = repo.text.indexOf("]");
                    var text = repo.text.substring(0,separatora);
                    var status = repo.text.substring(separatora+1,separatorb);
                    var markup = "<b style='font-size: 14px;'>" + repo.nmitem + " </b>" ;
                    return markup;
                }

                function formatSalesHeaderSelection (repo) {
                    var separatora = repo.text.indexOf("[");
                    var text = repo.text.substring(0,separatora);
                    return text;
                }
                //$('#kdsales_header').val($("#kdsaleshd").val()).trigger('change');
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });
    }   

    function clear(){
        $("#kditem").val('');
        $("#qty").val('');
        $("#harga").autoNumeric('set',0);
    }

    function sum(){
        var tot1 = $('#tot1').autoNumeric('get');
        var pot = $('#pot').autoNumeric('get');
        var byongkos = $('#byongkos').autoNumeric('get');
        var byr = $('#byr').autoNumeric('get');

        if(pot===''){
            var pot = 0;
            $('#pot').autoNumeric('set',0);
        } else {
            var pot = pot; 
        }

        if(byongkos===''){
            var byongkos = 0; 
            $('#byongkos').autoNumeric('set',0);
        } else {
            var byongkos = byongkos; 
        }

        var tot2 = parseFloat(tot1) - parseFloat(pot) + parseFloat(byongkos); 
        if(!isNaN(tot2)){
            $('#tot2').autoNumeric('set',tot2);
        } 

        if(byr===''){
            var byr = 0;
            $('#byr').autoNumeric('set',0);
        } else {
            var byr = byr; 
        }

        var tot_akhir = parseFloat(byr) - parseFloat(tot2);
        if(!isNaN(tot_akhir)){
            $('#tot_akhir').autoNumeric('set',tot_akhir);
        }
    }

    function edit(nourut,kditem,qty,harga){
        $("#nourut").val(nourut);

        $("#kditem").val(kditem);
        //$("#kdakun").val(nmakun);
        $("#kditem").trigger("change");
        $('#kditem').select2({
                      dropdownAutoWidth : true,
                      width: '100%'
                    });

        $("#qty").val(qty);
        $("#harga").autoNumeric('set',harga);

        $('#modal_transaksi').modal('toggle');
    }

    function addDetail(){

        var noitem_out = $("#noitem_out").val();
        var tglitem_out = $("#tglitem_out").val(); 
        var ket = $("#ket").val();
        var nourut = $("#nourut").val();
        var kditem = $("#kditem").val();
        var harga = $("#harga").autoNumeric('get');
        var qty = $("#qty").autoNumeric('get');

        $.ajax({
            type: "POST",
            url: "<?=site_url("itemout/addDetail");?>",
            data: {"noitem_out":noitem_out
                    ,"tglitem_out":tglitem_out 
                    ,"ket":ket
                    ,"nourut":nourut
                    ,"kditem":kditem
                    ,"qty":qty
                    ,"harga":harga  },

            success: function(resp){

                var obj = JSON.parse(resp);
                $.each(obj, function(key, data){
                    swal({
                        title: data.title,
                        text: data.msg,
                        type: data.tipe
                    });

                    if (data.tipe==="success"){
                        $("#nourut").val('');
                        $("#noitem_out").val(data.noitem_out);
                        $("#modal_transaksi").modal("hide");
                        refresh();
                        clear();
                    }else{

                    }
                });
            }, 
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                refresh();
            }
        });
    }

    function deleted(noitem_out,nourut){
        swal({
            title: "Konfirmasi Hapus Data!",
            text: "Data yang dihapus tidak dapat dikembalikan!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false
        }, function () {
            $.ajax({
                type: "POST",
                url: "<?=site_url("itemout/detaildeleted");?>",
                data: {"noitem_out":noitem_out,"nourut":nourut  },
                success: function(resp){
                    var obj = JSON.parse(resp);
                    $.each(obj, function(key, data){
                        swal({
                            title: data.title,
                            text: data.msg,
                            type: data.tipe
                        }, function(){
                            refresh();
                        });
                    });
                },
                error:function(event, textStatus, errorThrown) {
                    swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                }
            });
        });
    }

    function refresh(){
        table.ajax.reload();
    }

    function batal(){
        swal({
            title: "Konfirmasi Batal Transaksi!",
            text: "Data yang dibatalkan tidak disimpan !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false
        }, function () {
          window.location.href = '<?=site_url('itemout');?>';
        });
    }

    function submit(){
        swal({
            title: "Konfirmasi Simpan Transaksi!",
            text: "Data yang akan disimpan !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false
        }, function () {
            var nodo = $("#nodo").val();
            var noitem_out = $("#noitem_out").val();
            var tglitem_out = $("#tglitem_out").val();
            var ket = $("#ket").val();
            var pot = $("#pot").autoNumeric('get');
            var byongkos = $("#byongkos").autoNumeric('get');
            var byr = $("#byr").autoNumeric('get');
            $.ajax({
                type: "POST",
                url: "<?=site_url("itemout/save");?>",
                data: {"noitem_out":noitem_out
                    ,"tglitem_out":tglitem_out 
                    ,"ket":ket
                    ,"pot":pot
                    ,"byongkos":byongkos
                    ,"byr":byr },
                success: function(resp){
                    var obj = JSON.parse(resp);
                    $.each(obj, function(key, data){
                        swal({
                            title: data.title,
                            text: data.msg,
                            type: data.tipe
                        });
                        if(data.tipe==="success"){
                            window.location.href = '<?=site_url('itemout');?>';
                        }

                    });
                },
                error:function(event, textStatus, errorThrown) {
                    swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                }
            });
        });
    }

</script>
