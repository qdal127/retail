<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author :
 *  Email :
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Itemin
 *
 * @author
 */
class Itemin extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,

            'submit' => site_url('itemin/submit'),
            'add' => site_url('itemin/add'),
            'edit' => site_url('itemin/edit'),
            'reload' => site_url('itemin'),
        );
        $this->load->model('itemin_qry');
        $supplier = $this->itemin_qry->getDataSupplier();
        foreach ($supplier as $value) {
            $this->data['kdsup'][$value['kdsup']] = $value['nmsup'];
        }
    }

    //redirect if needed, otherwise display the user list

    public function index(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }

    public function add(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    }

    public function edit() {
        $this->_init_edit();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('form',$this->data);
    }

    public function printAll() {
        $this->data['data'] = $this->itemin_qry->printAll();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('print-layout')
            ->build('html',$this->data);
    }

    public function submit() {
        $this->data['data'] = $this->itemin_qry->submit();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('print-layout')
            ->build('html',$this->data); 
    }

    public function getKodeitemin() {
        echo $this->itemin_qry->getKodeitemin();
    }

    public function json_dgview() {
        echo $this->itemin_qry->json_dgview();
    }

    public function json_dgview_detail() {
        echo $this->itemin_qry->json_dgview_detail();
    }
    public function addDetail() {
        echo $this->itemin_qry->addDetail();
    }

    public function detaildeleted() {
        echo $this->itemin_qry->detaildeleted();
    }

    public function delete() {
        echo $this->itemin_qry->delete();
    }

    public function save() {
        echo $this->itemin_qry->save();
    }
    private function _init_add(){
        $this->data['form'] = array(
           'noitem_in'=> array(
                    'placeholder' => 'No. Transaksi Item / Obat Masuk',
                    //'type'        => 'hidden',
                    'id'          => 'noitem_in',
                    'name'        => 'noitem_in',
                    'value'       => set_value('noitem_in'),
                    'class'       => 'form-control',
                    'style'       => '',
                    'readonly'    => '',
            ),
           'tglitem_in'=> array(
                    'placeholder' => 'Tanggal Transaksi Item / Obat Masuk',
                    'id'          => 'tglitem_in',
                    'name'        => 'tglitem_in',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control calendar',
                    'style'       => '',
            ),
            'kdsup'=> array(
                    'attr'        => array(
                        'id'    => 'kdsup',
                        'class' => 'form-control  select',
                    ),
                    'data'     =>  $this->data['kdsup'],
                    'value'    => set_value('kdsup'),
                    'name'     => 'kdsup',
                    'required'    => '',
                    'placeholder' => 'Supplier',
            ),
           'nmsup'=> array(
                    'placeholder' => 'Supplier',
                    'id'          => 'nmsup',
                    'name'        => 'nmsup',
                    'value'       => set_value('nmsup'),
                    'class'       => 'form-control',
                    'required'    => '',
            //        'style'       => 'text-transform: uppercase;',
            ),
            'ket'=> array(
                    'placeholder' => 'Keterangan',
                    'id'      => 'ket',
                    'class' => 'form-control',
                    'data'     => '',
                    'value'    => set_value('ket'),
                    'name'     => 'ket',
            //        'required'    => '',
            //        'onkeyup'     => 'myFunction()',
            //        'style'       => 'text-transform: uppercase;',
            ),
            'nourut'=> array(
                    'id'    => 'nourut',
                    'type'    => 'hidden',
                    'class' => 'form-control',
                    'data'     => '',
                    'value'    => set_value('nourut'),
                    'name'     => 'nourut',
                    'required'    => ''
            ),
            'kditem'=> array(
                    'attr'        => array(
                        'id'    => 'kditem',
                        'class' => 'form-control',
                    ),
                    'data'     =>  '',
                    'value'    => set_value('kditem'),
                    'name'     => 'kditem',
                    'placeholder' => 'Cari Barang',
                    'required' => ''
            ),
            'nmitem'=> array(
                    'attr'        => array(
                        'id'    => 'nmitem',
                        'class' => 'form-control',
                    ),
                    'data'     => '',
                    'class' => 'form-control',
                    'value'    => set_value('nmitem'),
                    'name'     => 'nmitem',
                    'readonly' => '',
            ),
            'qty'=> array(
                    'placeholder' => 'Quantity',
                    'id'    => 'qty',
                    'class' => 'form-control',
                    'value'    => set_value('qty'),
                    'name'     => 'qty',
                    'required'    => '',
            ),
            'harga'=> array(
                    'id'    => 'harga',
                    'class' => 'form-control',
                    'value'    => set_value('harga'),
                    'name'     => 'harga',
            ),
           'tgltrans'=> array(
                    'placeholder' => 'Tanggal Transaksi',
                    'id'          => 'tgltrans',
                    'name'        => 'tgltrans',
                    'value'       => date('d-m-Y'),
                    'class'       => 'form-control calendar',
                    'style'       => '',
            ),
        );
    }

    private function _init_edit($no = null){
        if(!$no){
            $noitemin = $this->uri->segment(3);
        }
        $this->_check_id($noitemin);
        $this->data['form'] = array(
           'noitem_in'=> array(
                    'placeholder' => 'No. Transaksi Item / Obat Masuk',
                    //'type'        => 'hidden',
                    'id'          => 'noitem_in',
                    'name'        => 'noitem_in',
                    'value'       => $this->val[0]['noitem_in'],
                    'class'       => 'form-control',
                    'style'       => '',
                    'readonly'    => '',
            ),
           'tglitem_in'=> array(
                    'placeholder' => 'Tanggal Transaksi Item / Obat Masuk',
                    'id'          => 'tglitem_in',
                    'name'        => 'tglitem_in',
                    'value'       => $this->apps->dateConvert($this->val[0]['tglitem_in']),
                    'class'       => 'form-control calendar',
                    'style'       => '',
            ),
            'kdsup'=> array(
                    'attr'        => array(
                        'id'    => 'kdsup',
                        'class' => 'form-control  select',
                    ),
                    'data'     =>  $this->data['kdsup'],
                    'value'    => $this->val[0]['kdsup'],
                    'name'     => 'kdsup',
                    'required'    => '',
                    'placeholder' => 'Supplier',
            ),
           'nmsup'=> array(
                    'placeholder' => 'Supplier',
                    'id'          => 'nmsup',
                    'name'        => 'nmsup',
            //        'value'       => $this->val[0]['nmsup'],
                    'class'       => 'form-control',
                    'required'    => '',
            //        'style'       => 'text-transform: uppercase;',
            ),
            'ket'=> array(
                    'placeholder' => 'Keterangan',
                    'id'      => 'ket',
                    'class' => 'form-control',
                    'data'     => '',
                    'value'    => $this->val[0]['ket'],
                    'name'     => 'ket',
            //        'required'    => '',
            //        'onkeyup'     => 'myFunction()',
            //        'style'       => 'text-transform: uppercase;',
            ),
            'nourut'=> array(
                    'id'    => 'nourut',
                    'type'    => 'hidden',
                    'class' => 'form-control',
                    'data'     => '',
                    'value'    => set_value('nourut'),
                    'name'     => 'nourut',
                    'required'    => ''
            ),
            'kditem'=> array(
                    'attr'        => array(
                        'id'    => 'kditem',
                        'class' => 'form-control',
                    ),
                    'data'     =>  '',
                    'value'    => set_value('kditem'),
                    'name'     => 'kditem',
                    'placeholder' => 'Cari Barang',
                    'required' => ''
            ),
            'nmitem'=> array(
                    'attr'        => array(
                        'id'    => 'nmitem',
                        'class' => 'form-control',
                    ),
                    'data'     => '',
                    'class' => 'form-control',
                    'value'    => set_value('nmitem'),
                    'name'     => 'nmitem',
                    'readonly' => '',
            ),
            'qty'=> array(
                    'placeholder' => 'Quantity',
                    'id'    => 'qty',
                    'class' => 'form-control',
                    'value'    => set_value('qty'),
                    'name'     => 'qty',
                    'required'    => '',
            ),
            'harga'=> array(
                    'id'    => 'harga',
                    'class' => 'form-control',
                    'value'    => set_value('harga'),
                    'name'     => 'harga',
            ),
        );
    }

    private function _check_id($nojurnal){
        if(empty($nojurnal)){
            redirect($this->data['add']);
        }

        $this->val= $this->itemin_qry->select_data($nojurnal);

        if(empty($this->val)){
            redirect($this->data['add']);
        }
    }

    private function validate($noref,$ket) {
        if(!empty($noref) && !empty($ket)){
            return true;
        }
        $config = array(
            array(
                    'field' => 'kditem',
                    'label' => 'No Referensi',
                    'rules' => 'required',
                ),
            array(
                    'field' => 'ket',
                    'label' => 'Keterangan',
                    'rules' => 'required',
                    ),
        );

        echo "<script> console.log('validate: ". json_encode($config) ."');</script>";

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }

    private function validate_detail($nojurnal,$nourut) {
        if(!empty($nojurnal) && !empty($nourut)){
            return true;
        }
        $config = array(
            array(
                    'field' => 'nourut',
                    'label' => 'No. Urut',
                    'rules' => 'required',
                ),
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
}
