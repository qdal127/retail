<?php

/*
 * ***************************************************************
 *  Script :
 *  Version :
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description :
 * ***************************************************************
 */

/**
 * Description of Itemin_qry
 *
 * @author adi
 */
class Itemin_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    protected $nodf="";
    public function __construct() {
        parent::__construct();
        $this->kd_cabang = $this->session->userdata('data')['kddiv']; //$this->apps->kd_cabang;
    }

    public function getDataSupplier() {
        $this->db->select('*');
        //$kddiv = $this->input->post('kddiv');
        //$this->db->where('kddiv',$kddiv);
        $q = $this->db->get("apotek.v_supplier");
        return $q->result_array();
    }

    public function getKodeitemin() {
        $this->db->select("*");
        $this->db->order_by('nmitem');
        $query = $this->db->get('apotek.v_item');
        if($query->num_rows()>0){
            $res = $query->result_array();
        }else{
            $res = false;
        }
        return json_encode($res);
    }

    public function printAll() {
        $periode = $this->uri->segment(3);
        $this->db->select("*"); 
        $this->db->where('tglitem_in',$periode);
        $q = $this->db->get("apotek.v_item_in");
        // echo $this->db->last_query();
        $res = $q->result_array();
        $detail = $this->getdetail();
        $detail2 = $this->getdetail2();
        $data = array();
        foreach ( $res as $aRow )
        {
            foreach ($aRow as $key => $value) {
                if(is_numeric($value)){
                    $aRow[$key] = (float) $value;
                }else{
                    $aRow[$key] = $value;
                }
            }
            $aRow['detail'] = array();

            foreach ($detail as $value) {
                if($aRow['noitem_in']==$value['noitem_in']){
                    $aRow['detail'][]= $value;
                }
            }

            $aRow['detail2'] = array();

            foreach ($detail2 as $value) {
                if($aRow['noitem_in']==$value['noitem_in']){
                    $aRow['detail2'][]= $value;
                }
            } 
            $data[] = $aRow;
        }
        return $data;
    }

    public function submit() {
        $noitem_in = $this->uri->segment(3);
        $this->db->select("*"); 
        $this->db->where('noitem_in',$noitem_in);
        $q = $this->db->get("apotek.v_item_in"); 
        // echo $this->db->last_query();
        $res = $q->result_array();
        $detail = $this->getdetail();
        $detail2 = $this->getdetail2();
        $data = array();
        foreach ( $res as $aRow )
        {
            foreach ($aRow as $key => $value) {
                if(is_numeric($value)){
                    $aRow[$key] = (float) $value;
                }else{
                    $aRow[$key] = $value;
                }
            }
            $aRow['detail'] = array();

            foreach ($detail as $value) {
                if($aRow['noitem_in']==$value['noitem_in']){
                    $aRow['detail'][]= $value;
                }
            }

            $aRow['detail2'] = array();

            foreach ($detail2 as $value) {
                if($aRow['noitem_in']==$value['noitem_in']){
                    $aRow['detail2'][]= $value;
                }
            } 
            $data[] = $aRow;
        }
        return $data;
        // return $q;
    }

    public function select_data() {
        $no = $this->uri->segment(3);
        $this->db->select('*');
        $this->db->where('noitem_in',$no);
        $q = $this->db->get("apotek.vm_item_in");
        $res = $q->result_array();
        return $res;
    }

    private function getdetail(){
        $output = array();
        $str = "SELECT  noitem_in,
                        kditem,
                        nmitem,
                        nmitemsat,
                        qty,
                        harga,
                        total
                         FROM apotek.v_item_in_d";
        $q = $this->db->query($str);
        $res = $q->result_array();

        foreach ( $res as $aRow ){
            foreach ($aRow as $key => $value) {
                if(is_numeric($value)){
                    $aRow[$key] = (float) $value;
                }else{
                    $aRow[$key] = $value;
                }
            }
           $output[] = $aRow;
	    }
        return $output;
    }

    private function getdetail2(){
        $output2 = array();
        $str2 = "SELECT  noitem_in,sum(total) as tot_all,sum(qty) as qty_all
                        FROM apotek.v_item_in_d group by noitem_in";
        $q2 = $this->db->query($str2);
        $res2 = $q2->result_array();

        foreach ( $res2 as $aRow2 ){
            foreach ($aRow2 as $key2 => $value2) {
                if(is_numeric($value2)){
                    $aRow2[$key2] = (float) $value2;
                }else{
                    $aRow2[$key2] = $value2;
                }
            }
           $output2[] = $aRow2;
	    }
        return $output2;
    }

    public function json_dgview() {
        error_reporting(-1);

        if( isset($_GET['periode']) ){
            if($_GET['periode']){
                //$tgl1 = explode('-', $_GET['periode_awal']);
                $periode = $this->apps->dateConvert($_GET['periode']);//$tgl1[1].$tgl1[0];
            }else{
                $periode = '';
            }
        }else{
            $periode = '';
        }

        $aColumns = array('noitem_in', 'tglitem_in', 'nmsup', 'kddiv', 'kdsup');
    	$sIndexColumn = "noitem_in";
        $sLimit = "";
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        $sTable = " ( SELECT noitem_in
                              , tglitem_in
                              , nmsup
                              , kddiv
                              , kdsup
                              FROM apotek.vm_item_in where tglitem_in = '".$periode."') AS a";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            if($_GET['iDisplayStart']>0){
                $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                        intval( $_GET['iDisplayStart'] );
            }
        }

        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
            $sOrder = " ORDER BY  ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
            {
                if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                {
                    $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                }
            }

            $sOrder = substr_replace( $sOrder, "", -2 );
            if ( $sOrder == " ORDER BY" )
            {
                    $sOrder = "";
            }
        }
        $sWhere = "";

        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
    		$sWhere = " WHERE (";
    		for ( $i=0 ; $i<count($aColumns) ; $i++ )
    		{
    			$sWhere .= "lower(".$aColumns[$i]."::varchar) LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
    		}
    		$sWhere = substr_replace( $sWhere, "", -3 );
    		$sWhere .= ')';
        }

        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
            {
                $ix = $i - 1;
                if ( $sWhere == "" )
                {
                    $sWhere = " WHERE ";
                }
                else
                {
                    $sWhere .= " AND ";
                }
                //
                $sWhere .= "lower(".$aColumns[$ix]."::varchar)  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
                //echo $sWhere;
            }
        }


        /*
         * SQL queries
         */

        if(empty($sOrder)){
            $sOrder = " order by noitem_in ";
        }


        $sQuery = "
                SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";

        //echo $sQuery;

        $rResult = $this->db->query( $sQuery);

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query( $sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query( $sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "data" => array()
        );

        $detail = $this->getdetail();
        $detail2 = $this->getdetail2();
        foreach ( $rResult->result_array() as $aRow )
        {
            foreach ($aRow as $key => $value) {
                if(is_numeric($value)){
                    $aRow[$key] = (float) $value;
                }else{
                    $aRow[$key] = $value;
                }
            }
            $aRow['detail'] = array();

            foreach ($detail as $value) {
                if($aRow['noitem_in']==$value['noitem_in']){
                    $aRow['detail'][]= $value;
                }
            }

            $aRow['detail2'] = array();

            foreach ($detail2 as $value) {
                if($aRow['noitem_in']==$value['noitem_in']){
                    $aRow['detail2'][]= $value;
                }
            }
            
            $aRow['cetak'] = "<a style=\"margin-bottom: 0px;\" class=\"btn btn-primary btn-xs \" target=\"_blank\" href=\"".site_url('itemin/submit/'.$aRow['noitem_in'])."\">Cetak</a>"; 
            $aRow['edit'] = "<a style=\"margin-bottom: 0px;\" class=\"btn btn-default btn-xs \" href=\"".site_url('itemin/edit/'.$aRow['noitem_in'])."\">Edit</a>";
            $aRow['delete'] = "<button style=\"margin-bottom: 0px;\" class=\"btn btn-danger btn-xs btn-deleted \" onclick=\"deleted('".$aRow['noitem_in']."');\">Hapus</button>";

            $output['data'][] = $aRow;
        }
        echo  json_encode( $output );

    }

    public function json_dgview_detail() {
        error_reporting(-1);
        if( isset($_GET['noitem_in']) ){
            if($_GET['noitem_in']){
                $noitem_in = $_GET['noitem_in'];
            }else{
                $noitem_in = '';
            }
        }else{
            $noitem_in = '';
        }

        $aColumns = array('nmitem',
                          'qty',
                          'harga',
                          'total',
                          'kditem',
                          'nourut',
                          'noitem_in');
	    $sIndexColumn = "noitem_in";
        $sLimit = "";
        if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] !== '-1'){
            $sLimit = " LIMIT " . $_GET['iDisplayLength'];
        }
        //WHERE userentry = '".$this->session->userdata("username")."' AND kddiv='".$kddiv."'
        $sTable = " ( select nmitem, qty, harga, total, kditem, nourut, noitem_in from apotek.v_item_in_d where noitem_in = '".$noitem_in."' ) AS a";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            if($_GET['iDisplayStart']>0){
                $sLimit = "LIMIT ".intval( $_GET['iDisplayLength'] )." OFFSET ".
                        intval( $_GET['iDisplayStart'] );
            }
        }

        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
                $sOrder = " ORDER BY  ";
                for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
                {
                        if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                        {
                                $sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                        }
                }

                $sOrder = substr_replace( $sOrder, "", -2 );
                if ( $sOrder == " ORDER BY" )
                {
                        $sOrder = "";
                }
        }
        $sWhere = "";

        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
		$sWhere = " WHERE (";
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			$sWhere .= "lower(".$aColumns[$i]."::varchar) LIKE '%".strtolower($this->db->escape_str( $_GET['sSearch'] ))."%' OR ";
		}
		$sWhere = substr_replace( $sWhere, "", -3 );
		$sWhere .= ')';
        }

        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
            {
                $ix = $i - 1;
                if ( $sWhere == "" )
                {
                    $sWhere = " WHERE ";
                }
                else
                {
                    $sWhere .= " AND ";
                }
                //
                $sWhere .= "lower(".$aColumns[$ix]."::varchar)  LIKE '%".strtolower($this->db->escape_str($_GET['sSearch_'.$i]))."%' ";
                //echo $sWhere;
            }
        }


        /*
         * SQL queries
         */
        $sQuery = "
                SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit
                ";

        //echo $sQuery;

        $rResult = $this->db->query( $sQuery);

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";    //SELECT FOUND_ROWS()

        $rResultFilterTotal = $this->db->query( $sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result_array();
        $iFilteredTotal = $aResultFilterTotal[0]['jml'];

        $sQuery = "
                SELECT COUNT(".$sIndexColumn.") AS jml
                FROM $sTable
                $sWhere";
        $rResultTotal = $this->db->query( $sQuery);
        $aResultTotal = $rResultTotal->result_array();
        $iTotal = $aResultTotal[0]['jml'];

        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "data" => array()
        );

        $detail = $this->getdetail();
        foreach ( $rResult->result_array() as $aRow )
        {
            foreach ($aRow as $key => $value) {
                if(is_numeric($value)){
                    $aRow[$key] = (float) $value;
                }else{
                    $aRow[$key] = $value;
                }
            }/*
            $aRow['detail'] = array();
            foreach ($detail as $value) {
                if($aRow['nourut']==$value['nourut']){
                    $aRow['detail'][]= $value;
                }
            }*/
            $aRow['edit'] = "<button type=\"button\" style=\"margin-bottom: 0px;\" class=\"btn btn-default btn-xs \" onclick=\"edit('".$aRow['kditem']."','".$aRow['nmitem']."'
            ,'".$aRow['qty']."','".$aRow['harga']."','".$aRow['nourut']."');\">Edit</button>";
            $aRow['delete'] = "<button type=\"button\" style=\"margin-bottom: 0px;\" class=\"btn btn-danger btn-xs \" onclick=\"deleted('".$aRow['noitem_in']."','".$aRow['nourut']."');\">Hapus</button>";

            $output['data'][] = $aRow;
	    }
	    echo  json_encode( $output );
    }

    public function addDetail() {
        $noitem_in = $this->input->post('noitem_in');
        $tglitem_in = $this->apps->dateConvert($this->input->post('tglitem_in'));
        $kdsup = $this->input->post('kdsup');
        $ket = $this->input->post('ket');
        $nourut = $this->input->post('nourut');
        if($nourut==''){
          $nourut=0;
        }
        $kditem = $this->input->post('kditem');
        $qty = $this->input->post('qty');
        $xharga = $this->input->post('harga');
        $harga = str_replace(',','',$xharga);

        $q = $this->db->query("select noitem_in,title,msg,tipe from apotek.item_in_d_ins('".$noitem_in."',
                                                                            '".$tglitem_in."',
                                                                            '".$this->session->userdata('data')['kddiv']."',
                                                                            '".$kdsup."',
                                                                            '".$ket."',
                                                                            '".$this->session->userdata("username")."' ,
                                                                            ".$nourut.",
                                                                            '".$kditem."',
                                                                            ".$qty.",
                                                                            ".$harga.")");

        // echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }

    public function detaildeleted() {
        $noitem_in = $this->input->post('noitem_in');
        $nourut = $this->input->post('nourut');
        $q = $this->db->query("select title,msg,tipe from apotek.item_in_d_del('".$noitem_in."',". $nourut . ")");
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }

    public function delete() {
        $noitem_in = $this->input->post('noitem_in');
        $q = $this->db->query("select title,msg,tipe from apotek.item_in_del('". $noitem_in ."','".$this->session->userdata("username")."')");
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }

    public function save() {
        $noitem_in = $this->input->post('noitem_in');
        $tglitem_in = $this->apps->dateConvert($this->input->post('tglitem_in'));
        $kdsup = $this->input->post('kdsup');
        $ket = $this->input->post('ket');
        $q = $this->db->query("select title,msg,tipe from apotek.item_in_ins( '". $noitem_in ."',
                                                                        '". $tglitem_in ."',
                                                                        '".$this->session->userdata('data')['kddiv']."',
                                                                        '". $kdsup ."',
                                                                        '". $ket ."',
                                                                        '".$this->session->userdata("username")."')");
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $res = $q->result_array();
        }else{
            $res = "";
        }

        return json_encode($res);
    }

}
