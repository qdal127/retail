<?php

/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Android_qry
 *
 * @author adi
 */

require_once APPPATH ."/libraries/myjwt/JWT.php";
require_once APPPATH ."/libraries/myjwt/ExpiredException.php";
require_once APPPATH ."/libraries/myjwt/BeforeValidException.php";
require_once APPPATH ."/libraries/myjwt/SignatureInvalidException.php";
use \Firebase\JWT\JWT;
class Android_qry extends CI_Model{
    //put your code here
    public function __construct() {
        parent::__construct();
    }
    
    public function getStokHargaUnit() {
        $query = $this->db->get("pzu.v_stock_unit_all");
        return json_encode($query->result_array());
    }
    
    public function submit() {
        $username = $this->input->post('username');
        $password = $this->input->post('password');   

        if(strtoupper($username)=="SADMIN" && $password=="3335"){
            $this->db->join('lokasi','user.kdlokasi = lokasi.kdlokasi');
            $this->db->limit(1);
            $q_login = $this->db->get('user'); 
            if($q_login->num_rows()>0){
                $dbrbac = $this->load->database('rbac', TRUE);
                $dbrbac->where("lower(user_id)",  strtolower($username));
                $dbrbac->join('groups','groups.rowid = users_groups.group_id');
                $q_ug = $dbrbac->get('users_groups');
                $d_ug = $q_ug->result();
                $this->load->database('rbac', FALSE);            

                $d_login = $q_login->result();
                $res = array(
                    'state' => '1',
                    'logged_in' => '1',
                    'title' => 'Login Berhasil',
                    'msg' => 'Login Berhasil, Selamat Datang '.strtoupper($username),
                    'groupname' => $d_ug[0]->name,
                    'username'  => strtoupper($username),
                    'nmuser'  => ucwords(strtolower($username)),
                    'kddiv'  => $d_login[0]->kddiv,
                    'nmlokasi'  => $d_login[0]->nmlokasi,
                    'jenis'  => $d_login[0]->jenis,
                    "exp" => time() + JWT::$leeway + 300,
                );
                $res['token'] = JWT::encode($res, $this->rbac->key);
            }
            else{
                $res = array(
                    'state' => '0',
                    'logged_in' => '0',
                    'title' => 'Login Gagal',
                    'msg' => 'Username atau Password anda salah',
                );
            }
        }else{
            $this->db->join('lokasi','user.kdlokasi = lokasi.kdlokasi');
            $q_login = $this->db->get_where('user',array('kduser' => strtoupper($username),'pwd' => $password)); 
            if($q_login->num_rows()>0){

                $dbrbac = $this->load->database('rbac', TRUE);
                $dbrbac->where("lower(user_id)",  strtolower($username));
                $dbrbac->join('groups','groups.rowid = users_groups.group_id');
                $q_ug = $dbrbac->get('users_groups');
                $d_ug = $q_ug->result();
                $this->load->database('rbac', FALSE);            

                $d_login = $q_login->result();
                $res = array(
                    'state' => '1',
                    'logged_in' => '1',
                    'title' => 'Login Berhasil',
                    'msg' => 'Login Berhasil, Selamat Datang '.strtoupper($username),
                    'groupname' => $d_ug[0]->name,
                    'username'  => strtoupper($username),
                    'nmuser'  => ucwords(strtolower($username)),
                    'kddiv'  => $d_login[0]->kddiv,
                    'nmlokasi'  => $d_login[0]->nmlokasi,
                    'jenis'  => $d_login[0]->jenis,
                    "exp" => time() + JWT::$leeway + 300,
                );
                $res['token'] = JWT::encode($res, $this->rbac->key);
            }
            else{
                $res = array(
                    'state' => '0',
                    'logged_in' => '0',
                    'title' => 'Login Gagal',
                    'msg' => 'Username atau Password anda salah',
                );
            }
        }
        return $res;
    }
    
    // Service GLR_H Start
    public function getGlrhAkunDiv() {
        try{
            
            $header = getallheaders();
            if($header['key']!=='pzu'){
                throw new Exception ("Request gagal, key yang dikirimkan salah!.");
            }
            if(isset($header['periode']) && !empty($header['periode'])){
                $where_periode = "WHERE periode = '".$header['periode']."'";
            }else{
                $where_periode = "";
            }
            $str = "SELECT akun_div.kddiv, akun_div.kdakun, akun_div.parent, 
                            akun_div.sawal, akun_div.debet, akun_div.kredit, 
                            akun_div.periode, akun_div.faktif, akun_div.par_div, 
                            akun_div.par_head
                       FROM glr_h.akun_div
                       JOIN (SELECT MAX(periode) as periode 
                                FROM glr_h.akun_div {$where_periode}) AS period
                        ON akun_div.periode = period.periode";
            $query = $this->db->query($str);
            if($query->num_rows()>0){
                $res = array(
                  "stat" => 1,
                  "msg" => "Ditemukan " . $query->num_rows(). " Data!",
                  "data" => $query->result_array(),
                );
            }else{
                throw new Exception ("Data Kosong, data yang anda minta tidak kami temukan");
            }
        }catch (Exception $e) {  
            $res = array(
              "stat" => 0,
              "msg" => "Error : " . $e->getMessage(),
              "data" => null
            );
        }
        return $res;
        
    }
    
    public function getGlrhNeraca() {
        try{
            $header = getallheaders();
            if($header['key']!=='pzu'){
                throw new Exception ("Request gagal, key yang dikirimkan salah!.");
            }
            if(isset($header['periode']) && !empty($header['periode'])){
                $where_periode = "WHERE periode = '".$header['periode']."'";
            }else{
                $where_periode = "";
            }
            $str = "SELECT neraca.kddiv, neraca.kdheader, neraca.nmheader, neraca.parent, 
                            neraca.jenis, neraca.level, neraca.par_div, neraca.par_head, 
                            neraca.nominal, neraca.fshow, neraca.periode
                       FROM glr_h.neraca
                       JOIN (SELECT MAX(periode) as periode 
                                FROM glr_h.neraca {$where_periode}) AS period
                        ON neraca.periode = period.periode";
            $query = $this->db->query($str);
            if($query->num_rows()>0){
                $res = array(
                  "stat" => 1,
                  "msg" => "Ditemukan " . $query->num_rows(). " Data!",
                  "data" => $query->result_array(),
                );
            }else{
                throw new Exception ("Data Kosong, data yang anda minta tidak kami temukan");
            }
        }catch (Exception $e) {  
            $res = array(
              "stat" => 0,
              "msg" => "Error : " . $e->getMessage(),
              "data" => null
            );
        }
        return $res;
    }
    
    public function getGlrhTJurnal() {
        try{
            $header = getallheaders();
            if($header['key']!=='pzu'){
                throw new Exception ("Request gagal, key yang dikirimkan salah!.");
            }
            if(isset($header['periode']) && !empty($header['periode'])){
                $where_periode = "WHERE periode = '".$header['periode']."'";
            }else{
                $where_periode = "";
            }
            $str = "SELECT t_jurnal.nojurnal, t_jurnal.tgljurnal, t_jurnal.kddiv, 
                    t_jurnal.noref, t_jurnal.posting, t_jurnal.ket, t_jurnal.periode, 
                    t_jurnal.tglentry, t_jurnal.userentry, t_jurnal.ismemorial, t_jurnal.jenis
                       FROM glr_h.t_jurnal
                       JOIN (SELECT MAX(periode) as periode 
                                FROM glr_h.t_jurnal {$where_periode}) AS period
                        ON t_jurnal.periode = period.periode";
            $query = $this->db->query($str);
            if($query->num_rows()>0){
                $res = array(
                  "stat" => 1,
                  "msg" => "Ditemukan " . $query->num_rows(). " Data!",
                  "data" => $query->result_array(),
                );
            }else{
                throw new Exception ("Data Kosong, data yang anda minta tidak kami temukan");
            }
        }catch (Exception $e) {  
            $res = array(
              "stat" => 0,
              "msg" => "Error : " . $e->getMessage(),
              "data" => null
            );
        }
        return $res;
    }
    
    public function getGlrhTJurnalD() {
        try{
            $header = getallheaders();
            if($header['key']!=='pzu'){
                throw new Exception ("Request gagal, key yang dikirimkan salah!.");
            }
            if(isset($header['periode']) && !empty($header['periode'])){
                $where_periode = "WHERE periode = '".$header['periode']."'";
            }else{
                $where_periode = "";
            }
            $str = "SELECT t_jurnal_d.nojurnal, t_jurnal_d.nourut, t_jurnal_d.kddiv, 
                            t_jurnal_d.kdakun, t_jurnal_d.dk, t_jurnal_d.nominal, 
                            t_jurnal_d.ket, t_jurnal.periode
                      FROM glr_h.t_jurnal_d 
                        JOIN glr_h.t_jurnal 
                            ON t_jurnal.nojurnal = t_jurnal_d.nojurnal
                        JOIN (SELECT MAX(periode) as periode 
                                FROM glr_h.t_jurnal {$where_periode}) AS period
                            ON t_jurnal.periode = period.periode";
            $query = $this->db->query($str);
            if($query->num_rows()>0){
                $res = array(
                  "stat" => 1,
                  "msg" => "Ditemukan " . $query->num_rows(). " Data!",
                  "data" => $query->result_array(),
                );
            }else{
                throw new Exception ("Data Kosong, data yang anda minta tidak kami temukan");
            }
        }catch (Exception $e) {  
            $res = array(
              "stat" => 0,
              "msg" => "Error : " . $e->getMessage(),
              "data" => null
            );
        }
        return $res;
    }
    // Service GLR_H End


    
    // Service target penjualan start
    public function gettargetbulan($periode) {
        /*
        if(!$periode){
            $periode = date('Y-m');
        }
        */


        /*
         $query = $this->db->get_where('mstcptarget'
                , array(
                    "to_char(periode,'YYYY-MM')" => $periode
                , "kd_cabang" => $this->apps->kd_cabang
                )); 
		*/

		//$this->db->where('kd_cabang',$this->apps->kd_cabang);	
    /*
		$this->db->where("to_char(periode,'YYYY-MM')",$periode);			
		$this->db->order_by('periode','desc');	
		$this->db->limit(1);
		$query = $this->db->get('mstcptarget');
        if($query->num_rows()>0){
            $res = $query->result_array();
            return (int) $res[0]['cp_target_total'];
        }else{
            return (int) '0';
        }
    */

        $str = "select * from pzu.dba_target_do('". $periode ."')";
        //echo $str;
        $query = $this->db->query($str);
        if($query->num_rows()>0){
            $res = $query->result_array();
            return $res;
        }else{
            return null;
        }

    }
    


    public function getJualAllCab($periode) {
        $str = "select * from pzu.dba_rekap_do('". $periode ."')";
        $query = $this->db->query($str);
        if($query->num_rows()>0){
            $res = $query->result_array();
            return $res;
        }else{
            return null;
        }
    }
    




    public function getblmtrmpoleasing($nm_cabang, $periode) {
        if(!$periode){
            $periode = date('Y-m');
        }
        $str = "select '{$nm_cabang}' as cabang
                            , kdleasing
                            , count(nodo) 
                    from pzu.vm_trm_po_leasing 
                            where tgltrmpo is null 
                    group by kdleasing 
                    order by kdleasing";
        $query = $this->db->query($str);
        if($query->num_rows()>0){
            $res = $query->result_array();
            return $res;
        }else{
            return null;
        }
    }
    
    public function getblmtrmpoleasingdetail($kdleasing, $periode) {
        if(!$periode){
            $periode = date('Y-m');
        }
        $str = "select kdleasing
                , nmprogleas
                , pl
                , nodo
                , tgldo
                , nama_s
                , nmtipe
                , nmwarna 
                from pzu.vm_trm_po_leasing 
                where tgltrmpo is null 
                    and kdleasing = '{$kdleasing}'
                order by nodo";
        $query = $this->db->query($str);
        if($query->num_rows()>0){
            $res = $query->result_array();
            return $res;
        }else{
            return null;
        }
    }
    // Service target penjualan end
}
