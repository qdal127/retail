<?php

/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Dashboard_qry
 *
 * @author adi
 */
class Dashboard_qry extends CI_Model{
    //put your code here
    public function __construct() {
        parent::__construct();
    }
    public function getJualPerTahun() {
        $this->db->select("COUNT(nodo) jml, to_char(tgldo,'YYYY') tahun");        
        $this->db->group_by("to_char(tgldo,'YYYY')");
        $this->db->order_by("to_char(tgldo,'YYYY')");
        $query = $this->db->get("pzu.vl_jual");
        return json_encode($query->result_array());
    }
    
    public function getJualPerBulan() {
        $tahun = $this->input->post('tahun');
        $this->db->select("COUNT(nodo) AS jml, idbln.bulan");
        $this->db->group_by("idbln.bulan");
        $this->db->order_by("idbln.bulan");
        $this->db->join("(SELECT to_char(tgldo,'MM') AS bulan FROM pzu.vl_jual GROUP BY to_char(tgldo,'MM')) AS idbln","idbln.bulan = to_char(vl_jual.tgldo,'MM')","RIGHT");
        $query = $this->db->get("(SELECT * FROM pzu.vl_jual WHERE (to_char(tgldo,'YYYY') = '".$tahun."')) AS vl_jual");
        $res = array();
        foreach ($query->result_array() as $value) {
            $res[]=array(
                "jml" => $value['jml'],
                "bulan" => (int) $value['bulan'],
            );
        }
        return json_encode($res);  
    }
    
    public function getStokPerTipeAktual() {
        $tanggal = $this->input->post('tanggal');
        $tg = explode("-", $tanggal);
        $dt = $tg[1]."-".$tg[0];
//        $this->db->where("to_char(tglpo,'YYYY-MM')",$dt);
//        $this->db->where("(tgldo is null OR to_char(tgldo,'YYYY-MM') > '".$dt."')");
        $this->db->select("COUNT(nosin) AS jml"
        . ", CASE "
                . " WHEN tipe_group.nmtipegrp = 'CUB LOW' THEN 'CUB L'"
                . " WHEN tipe_group.nmtipegrp = 'CUB MID' THEN 'CUB M'"
                . " WHEN tipe_group.nmtipegrp = 'CUB HIGH' THEN 'CUB H'"
                . " WHEN tipe_group.nmtipegrp = 'AT LOW' THEN 'AT L'"
                . " WHEN tipe_group.nmtipegrp = 'AT MID' THEN 'AT M'"
                . " WHEN tipe_group.nmtipegrp = 'AT HIGH' THEN 'AT H'"
                . " WHEN tipe_group.nmtipegrp = 'SPORT LOW' THEN 'SPORT L'"
                . " WHEN tipe_group.nmtipegrp = 'SPORT MID' THEN 'SPORT M'"
                . " WHEN tipe_group.nmtipegrp = 'SPORT HIGH' THEN 'SPORT H'"
        . " ELSE 'LAIN' END AS nmtipegrp");
        $this->db->group_by("nmtipegrp");
        $this->db->order_by("nmtipegrp","ASC");
        $this->db->order_by("CASE WHEN nmtipegrp like '%LOW%' THEN 1 WHEN nmtipegrp like '%MID%' THEN 2 WHEN nmtipegrp like '%HIGH%' THEN 3 ELSE 0 END","ASC");
        $this->db->join("pzu.tipe","v_stock_unit_harian.kode = tipe.kode");
        $this->db->join("pzu.tipe_group","tipe.kdtipegrp = tipe_group.kdtipegrp","RIGHT");
        $query = $this->db->get("(SELECT * FROM pzu.v_stock_unit_harian WHERE to_char(tglpo,'YYYY-MM') = '".$dt."' AND (tgldo is null OR to_char(tgldo,'YYYY-MM') > '".$dt."')) AS v_stock_unit_harian");
        //// echo $this->db->last_query();
        $res = array();
        foreach ($query->result_array() as $value) {
            $res[]=array(
                "jml" => $value['jml'],
                "nmtipegrp" => $value['nmtipegrp'],
            );
        }
        return json_encode($res); 
        
    }
    
    public function getJualPerSalesAktual() {
        $tanggal = $this->input->post('tanggal');
        $this->db->where("to_char(tgldo,'MM-YYYY')",$tanggal);
        $this->db->where("(nmsales <> '-' AND nmsales <> '')");
        $this->db->select("COUNT(nodo) AS jml, nmsales");
        $this->db->group_by("nmsales");
        $this->db->order_by("COUNT(nodo)","DESC");
        $this->db->limit(10);
        $query = $this->db->get("pzu.vl_jual");
        $res = array();
        foreach ($query->result_array() as $value) {
            $res[]=array(
                "jml" => $value['jml'],
                "nmsales" => $value['nmsales'],
            );
        }
        return json_encode($res);         
    }
    
    public function getJualPerKecamatan() {
        $tanggal = $this->input->post('tanggal');
        $this->db->where("to_char(tgldo,'MM-YYYY')",$tanggal);
        $this->db->where("(kec <> '-' AND kec <> '')");
        $this->db->select("COUNT(nodo) AS jml, kec");
        $this->db->group_by("kec");
        $this->db->order_by("COUNT(nodo)","DESC");
        $this->db->limit(10);
        $query = $this->db->get("pzu.vl_jual");
        $res = array();
        foreach ($query->result_array() as $value) {
            $res[]=array(
                "jml" => $value['jml'],
                "kec" => $value['kec'],
            );
        }
        return json_encode($res); 
    }
    
    public function getJualSalesMTD() {
        $tanggal = $this->input->post('tanggal');
        $nama =  $this->input->post('nama');
        $data = array(
            'grafik' => $this->_getJualSalesGrafik($nama,$tanggal),
        );
        return json_encode($data);          
    }
    
    private function _getJualSalesGrafik($nama,$tanggal) {
        $str = "SELECT COUNT(nodo) AS jml
                    ,sales.nmsales as sv
                    FROM pzu.vl_jual
                    JOIN pzu.sales ON vl_jual.nmsales = sales.nmsales
                    JOIN pzu.sales AS sales_sv ON sales.kdsales_header = sales_sv.kdsales
                    JOIN pzu.sales AS sales_kc ON sales_sv.kdsales_header = sales_kc.kdsales
                            WHERE to_char(vl_jual.tgldo,'MM-YYYY') = '".$tanggal."'
                                AND sales_sv.nmsales = '".$nama."'
                            AND (sales_kc.nmsales like '%IMRON%' 
                                    OR sales_kc.nmsales like '%INDRA%'
                                    OR sales_kc.nmsales like '%ARIF YUDI SUSANTO%')
                            AND (sales_sv.nmsales not like '%IMRON FATONI (SALES)%' 
                                    AND sales_sv.nmsales not like 'ANDHI')
                    GROUP BY sales.nmsales
                    ORDER BY COUNT(nodo) DESC;";
        $query = $this->db->query($str);
        $res = array();
        foreach ($query->result_array() as $value) {
            $res[]=array(
                "jml" => $value['jml'],
                "kc" => $value['sv'],
            );
        }
        return $res; 
    }
    
    public function getJualSalesKoorMTD() {
        $tanggal = $this->input->post('tanggal');
        $nama =  $this->input->post('nama');
        $data = array(
            'grafik' => $this->_getJualSalesKoorGrafik($nama,$tanggal),
        );
        return json_encode($data);  
        
    }
    
    private function _getJualSalesKoorGrafik($nama,$tanggal) {
        $str = "SELECT COUNT(nodo) AS jml
                    ,sales_sv.nmsales as sv
                    FROM pzu.vl_jual
                    JOIN pzu.sales ON vl_jual.nmsales = sales.nmsales
                    JOIN pzu.sales AS sales_sv ON sales.kdsales_header = sales_sv.kdsales
                    JOIN pzu.sales AS sales_kc ON sales_sv.kdsales_header = sales_kc.kdsales
                            WHERE to_char(vl_jual.tgldo,'MM-YYYY') = '".$tanggal."'
                                AND CASE WHEN sales_kc.nmsales like '%IMRON FATONI%' THEN 'IMRON FATONI'
                                                WHEN sales_kc.nmsales like '%INDRA KURNIAWAN%' THEN 'INDRA KURNIAWAN' 
                                                ELSE sales_kc.nmsales END = '".$nama."'
                            AND (sales_kc.nmsales like '%IMRON%' 
                                    OR sales_kc.nmsales like '%INDRA%'
                                    OR sales_kc.nmsales like '%ARIF YUDI SUSANTO%')
                            AND (sales_sv.nmsales not like '%IMRON FATONI (SALES)%' 
                                    AND sales_sv.nmsales not like 'ANDHI')
                    GROUP BY sales_sv.nmsales,CASE WHEN sales_kc.nmsales like '%IMRON FATONI%' THEN 'IMRON FATONI'
                                                WHEN sales_kc.nmsales like '%INDRA KURNIAWAN%' THEN 'INDRA KURNIAWAN' 
                                                ELSE sales_kc.nmsales END
                    ORDER BY COUNT(nodo) DESC;";
        $query = $this->db->query($str);
        $res = array();
        foreach ($query->result_array() as $value) {
            $res[]=array(
                "jml" => $value['jml'],
                "kc" => $value['sv'],
            );
        }
        return $res; 
    }
    
    public function getJualSalesHeader() {
        $tahun = $this->input->post('tahun');
        $tanggal = $this->input->post('tanggal');
        $data = array(
            'salesheaderytd' => $this->_getJualSalesYTD($tahun),
            'salesheadermtd' => $this->_getJualSalesMTD($tanggal),
        );
        return json_encode($data);        
    }
    
    private function _getJualSalesYTD($tahun) {
        $str = "SELECT COUNT(nodo) AS jml
                    , CASE WHEN sales_kc.nmsales like '%IMRON FATONI%' THEN 'IMRON FATONI'
                            WHEN sales_kc.nmsales like '%INDRA KURNIAWAN%' THEN 'INDRA KURNIAWAN' 
                            ELSE sales_kc.nmsales END AS kc
                    FROM pzu.vl_jual
                    JOIN pzu.sales ON vl_jual.nmsales = sales.nmsales
                    JOIN pzu.sales AS sales_sv ON sales.kdsales_header = sales_sv.kdsales
                    JOIN pzu.sales AS sales_kc ON sales_sv.kdsales_header = sales_kc.kdsales
                            WHERE to_char(vl_jual.tgldo,'YYYY') = '".$tahun."'
                            AND (sales_kc.nmsales like '%IMRON%' 
                                    OR sales_kc.nmsales like '%INDRA%'
                                    OR sales_kc.nmsales like '%ARIF YUDI SUSANTO%')
                            AND (sales_sv.nmsales not like '%IMRON FATONI (SALES)%' 
                                    AND sales_sv.nmsales not like 'ANDHI')
                    GROUP BY CASE WHEN sales_kc.nmsales like '%IMRON FATONI%' THEN 'IMRON FATONI'
                            WHEN sales_kc.nmsales like '%INDRA KURNIAWAN%' THEN 'INDRA KURNIAWAN' 
                            ELSE sales_kc.nmsales END
                    ORDER BY CASE WHEN sales_kc.nmsales like '%IMRON FATONI%' THEN 'IMRON FATONI'
                            WHEN sales_kc.nmsales like '%INDRA KURNIAWAN%' THEN 'INDRA KURNIAWAN' 
                            ELSE sales_kc.nmsales END;";
        $query = $this->db->query($str);
        $res = array();
        foreach ($query->result_array() as $value) {
            $res[]=array(
                "jml" => $value['jml'],
                "kc" => $value['kc'],
            );
        }
        return $res; 
        
    }
    
    private function _getJualSalesMTD($tanggal) {
        $str = "SELECT COUNT(nodo) AS jml
                    , CASE WHEN sales_kc.nmsales like '%IMRON FATONI%' THEN 'IMRON FATONI'
                            WHEN sales_kc.nmsales like '%INDRA KURNIAWAN%' THEN 'INDRA KURNIAWAN' 
                            ELSE sales_kc.nmsales END AS kc
                    FROM pzu.vl_jual
                    JOIN pzu.sales ON vl_jual.nmsales = sales.nmsales
                    JOIN pzu.sales AS sales_sv ON sales.kdsales_header = sales_sv.kdsales
                    JOIN pzu.sales AS sales_kc ON sales_sv.kdsales_header = sales_kc.kdsales
                            WHERE to_char(vl_jual.tgldo,'MM-YYYY') = '".$tanggal."'
                            AND (sales_kc.nmsales like '%IMRON%' 
                                    OR sales_kc.nmsales like '%INDRA%'
                                    OR sales_kc.nmsales like '%ARIF YUDI SUSANTO%')
                            AND (sales_sv.nmsales not like '%IMRON FATONI (SALES)%' 
                                    AND sales_sv.nmsales not like 'ANDHI')
                    GROUP BY CASE WHEN sales_kc.nmsales like '%IMRON FATONI%' THEN 'IMRON FATONI'
                            WHEN sales_kc.nmsales like '%INDRA KURNIAWAN%' THEN 'INDRA KURNIAWAN' 
                            ELSE sales_kc.nmsales END
                    ORDER BY CASE WHEN sales_kc.nmsales like '%IMRON FATONI%' THEN 'IMRON FATONI'
                            WHEN sales_kc.nmsales like '%INDRA KURNIAWAN%' THEN 'INDRA KURNIAWAN' 
                            ELSE sales_kc.nmsales END;";
        $query = $this->db->query($str);
        $res = array();
        foreach ($query->result_array() as $value) {
            $res[]=array(
                "jml" => $value['jml'],
                "kc" => $value['kc'],
            );
        }
        return $res; 
        
    }
    
    public function getJualLeasing() {
        $tanggal = $this->input->post('tanggal');
        $this->db->select("COUNT(nodo) AS jml, kdleasing");
        $this->db->where("to_char(tgldo,'MM-YYYY')",$tanggal);
        $this->db->where("tk","K");
        $this->db->group_by("kdleasing");
        $this->db->order_by("kdleasing");
        $query = $this->db->get("pzu.vl_jual");
        $res = array();
        foreach ($query->result_array() as $value) {
            $res[]=array(
                "jml" => $value['jml'],
                "kdleasing" => $value['kdleasing'],
            );
        }
        return json_encode($res);  
    }
    
    public function getJualHarian() {
        $tanggal = $this->input->post('tanggal');
        $data = array(
            'getharian' => $this->_getHarian($tanggal),
            'gettarget' => $this->_getTarget($tanggal),
        );
        return json_encode($data);
    }  
    
    private function _getHarian($tanggal){
        $this->db->select("COUNT(nodo) jml, idtgl.tgl");        
        $this->db->group_by("idtgl.tgl");
        $this->db->order_by("idtgl.tgl");
        $this->db->join("(SELECT to_char(tgldo,'DD') AS tgl FROM pzu.vl_jual GROUP BY to_char(tgldo,'DD')) AS idtgl","idtgl.tgl = vl_jual.tgl","RIGHT");
        $query = $this->db->get("(SELECT nodo,to_char(tgldo,'DD') tgl FROM pzu.vl_jual WHERE (to_char(tgldo,'MM-YYYY') = '".$tanggal."')) AS vl_jual");
        $res = array();
        foreach ($query->result_array() as $value) {
            $res[]=array(
                "jml" => $value['jml'],
                "tgl" => $value['tgl'],
            );
        }
        return $res;  
        
    }
    
    private function _getTarget($tanggal){
        $this->db->select("id, kd_cabang, to_char(periode,'MM-YYYY') AS periode, jml_hr_kerja, cp_target_total, cp_target_harian");        
        $this->db->where("to_char(periode,'MM-YYYY')",$tanggal);
        $query = $this->db->get("pzu.mstcptarget");
        $res = array();
        if($query->num_rows()>0){
            foreach ($query->result_array() as $value) {
                $res[]=array(
                    "jml" => $value['cp_target_harian'],
                    "tgl" => $value['periode'],
                );
            }    
        }else{
            $res[]=array(
                "jml" => 0,
                "tgl" => $tanggal,
            );
            
        }
        return $res;  
        
    }
    
    public function getJualTipe() {
        $tahun = $this->input->post('tahun');
        $data = array(
            'pertipebar' => $this->_getJualPerTipeBar($tahun),
            'alltipepie' => $this->_getJualAllTipePie($tahun),
        );
        return json_encode($data);
    }    
    
    private function _getJualPerTipeBar($tahun) {
        $this->db->select("COUNT(nodo) AS jml"
                . ", CASE "
                        . " WHEN tipe_group.nmtipegrp = 'CUB LOW' THEN 'CUB L'"
                        . " WHEN tipe_group.nmtipegrp = 'CUB MID' THEN 'CUB M'"
                        . " WHEN tipe_group.nmtipegrp = 'CUB HIGH' THEN 'CUB H'"
                        . " WHEN tipe_group.nmtipegrp = 'AT LOW' THEN 'AT L'"
                        . " WHEN tipe_group.nmtipegrp = 'AT MID' THEN 'AT M'"
                        . " WHEN tipe_group.nmtipegrp = 'AT HIGH' THEN 'AT H'"
                        . " WHEN tipe_group.nmtipegrp = 'SPORT LOW' THEN 'SPORT L'"
                        . " WHEN tipe_group.nmtipegrp = 'SPORT MID' THEN 'SPORT M'"
                        . " WHEN tipe_group.nmtipegrp = 'SPORT HIGH' THEN 'SPORT H'"
                . " ELSE 'LAIN' END AS nmtipegrp");
        $this->db->group_by("tipe_group.nmtipegrp");
        $this->db->order_by("tipe_group.nmtipegrp");
        $this->db->join("pzu.tipe","vl_jual.kode = tipe.kode");
        $this->db->join("pzu.tipe_group","tipe.kdtipegrp = tipe_group.kdtipegrp","RIGHT");
        $query = $this->db->get("(SELECT * FROM pzu.vl_jual WHERE (to_char(tgldo,'YYYY') = '".$tahun."')) AS vl_jual");
        $res = array();
        foreach ($query->result_array() as $value) {
            $res[]=array(
                "jml" => $value['jml'],
                "nmtipegrp" => $value['nmtipegrp'],
            );
        }
        return $res;  
    }
    
    private function _getJualAllTipePie($tahun) {
        $this->db->select("SUM(CASE WHEN tipe_group.nmtipegrp LIKE 'AT%' THEN 1 
                                    WHEN tipe_group.nmtipegrp LIKE 'CUB%' THEN 1 
                                    WHEN tipe_group.nmtipegrp LIKE 'SPORT%' THEN 1 
                                    ELSE 0 END) AS jml
                            ,CASE WHEN tipe_group.nmtipegrp LIKE 'AT%' THEN 'AT' 
                                WHEN tipe_group.nmtipegrp LIKE 'CUB%' THEN 'CUB' 
                                WHEN tipe_group.nmtipegrp LIKE 'SPORT%' THEN 'SPORT'
                                ELSE 'LAINNYA' END AS nmtipegrp");
        $this->db->group_by("CASE WHEN tipe_group.nmtipegrp LIKE 'AT%' THEN 'AT' 
                                WHEN tipe_group.nmtipegrp LIKE 'CUB%' THEN 'CUB' 
                                WHEN tipe_group.nmtipegrp LIKE 'SPORT%' THEN 'SPORT'
                                ELSE 'LAINNYA' END");
        $this->db->order_by("CASE WHEN tipe_group.nmtipegrp LIKE 'AT%' THEN 'AT' 
                                WHEN tipe_group.nmtipegrp LIKE 'CUB%' THEN 'CUB' 
                                WHEN tipe_group.nmtipegrp LIKE 'SPORT%' THEN 'SPORT'
                                ELSE 'LAINNYA' END");
        $this->db->join("pzu.tipe","vl_jual.kode = tipe.kode");
        $this->db->join("pzu.tipe_group","tipe.kdtipegrp = tipe_group.kdtipegrp");
        $query = $this->db->get("(SELECT * FROM pzu.vl_jual WHERE (to_char(tgldo,'YYYY') = '".$tahun."')) AS vl_jual");
        $res = array();
        foreach ($query->result_array() as $value) {
            $res[]=array(
                "jml" => $value['jml'],
                "nmtipegrp" => $value['nmtipegrp'],
            );
        }
        return $res;   
    }
    
    public function getJualTunaiKreditPerBulan() {
        $tahun = $this->input->post('tahun');
        $tanggal = $this->input->post('tanggal');
        $data = array(
            'tunai' => $this->_getJualTunaiPerBulan($tahun),
            'kredit' => $this->_getJualKreditPerBulan($tahun),
            'tunaiytd' => $this->_getJualTunaiYTD($tahun),
            'kreditytd' => $this->_getJualKreditYTD($tahun),
            'tunaimtd' => $this->_getJualTunaiMTD($tanggal),
            'kreditmtd' => $this->_getJualKreditMTD($tanggal),
        );
        return json_encode($data);
    }
    
    private function _getJualTunaiPerBulan($tahun) {
        $this->db->select("COUNT(nodo) AS jml, idbln.bulan");
        $this->db->group_by("idbln.bulan");
        $this->db->order_by("idbln.bulan");
        $this->db->join("(SELECT to_char(tgldo,'MM') AS bulan FROM pzu.vl_jual GROUP BY to_char(tgldo,'MM')) AS idbln","idbln.bulan = to_char(vl_jual.tgldo,'MM')","RIGHT");
        $query = $this->db->get("(SELECT * FROM pzu.vl_jual WHERE tk = 'T' AND (to_char(tgldo,'YYYY') = '".$tahun."')) AS vl_jual");
        $res = array();
        foreach ($query->result_array() as $value) {
            $res[]=array(
                "jml" => $value['jml'],
                "bulan" => (int) $value['bulan'],
            );
        }
        return $res;  
    }
    
    private function _getJualKreditPerBulan($tahun) {
        $this->db->select("COUNT(nodo) AS jml, idbln.bulan");
        $this->db->group_by("idbln.bulan");
        $this->db->order_by("idbln.bulan");
        $this->db->join("(SELECT to_char(tgldo,'MM') AS bulan FROM pzu.vl_jual GROUP BY to_char(tgldo,'MM')) AS idbln","idbln.bulan = to_char(vl_jual.tgldo,'MM')","RIGHT");
        $query = $this->db->get("(SELECT * FROM pzu.vl_jual WHERE tk = 'K' AND (to_char(tgldo,'YYYY') = '".$tahun."')) AS vl_jual");
        $res = array();
        foreach ($query->result_array() as $value) {
            $res[]=array(
                "jml" => $value['jml'],
                "bulan" => (int) $value['bulan'],
            );
        }
        return $res;  
        
    }
    
    private function _getJualTunaiYTD($tahun) {
        $this->db->select("COUNT(nodo) AS jml, vl_jual.tk");
        $this->db->group_by("vl_jual.tk");
        $this->db->order_by("vl_jual.tk");
        $query = $this->db->get("(SELECT * FROM pzu.vl_jual WHERE tk = 'T' AND (to_char(tgldo,'YYYY') = '".$tahun."')) AS vl_jual");
        $res = array();
        foreach ($query->result_array() as $value) {
            $res[]=array(
                "jml" => $value['jml'],
                "tk" => $value['tk'],
            );
        }
        return $res;  
    }
    
    private function _getJualKreditYTD($tahun) {
        $this->db->select("COUNT(nodo) AS jml, vl_jual.tk");
        $this->db->group_by("vl_jual.tk");
        $this->db->order_by("vl_jual.tk");
        $query = $this->db->get("(SELECT * FROM pzu.vl_jual WHERE tk = 'K' AND (to_char(tgldo,'YYYY') = '".$tahun."')) AS vl_jual");
        $res = array();
        foreach ($query->result_array() as $value) {
            $res[]=array(
                "jml" => $value['jml'],
                "tk" => $value['tk'],
            );
        }
        return $res;  
        
    }
    
    private function _getJualTunaiMTD($tanggal) {
        $this->db->select("COUNT(nodo) AS jml, vl_jual.tk");
        $this->db->group_by("vl_jual.tk");
        $this->db->order_by("vl_jual.tk");
        $query = $this->db->get("(SELECT * FROM pzu.vl_jual WHERE tk = 'T' AND (to_char(tgldo,'MM-YYYY') = '".$tanggal."')) AS vl_jual");
        $res = array();
        foreach ($query->result_array() as $value) {
            $res[]=array(
                "jml" => $value['jml'],
                "tk" => $value['tk'],
            );
        }
        return $res;  
    }
    
    private function _getJualKreditMTD($tanggal) {
        $this->db->select("COUNT(nodo) AS jml, vl_jual.tk");
        $this->db->group_by("vl_jual.tk");
        $this->db->order_by("vl_jual.tk");
        $query = $this->db->get("(SELECT * FROM pzu.vl_jual WHERE tk = 'K' AND (to_char(tgldo,'MM-YYYY') = '".$tanggal."')) AS vl_jual");
        $res = array();
        foreach ($query->result_array() as $value) {
            $res[]=array(
                "jml" => $value['jml'],
                "tk" => $value['tk'],
            );
        }
        return $res;  
        
    }
    
    public function getJualJenisPerBulan() {
        $tahun = $this->input->post('tahun');
        $data = array(
            'tunai' => $this->_getDetailJualJenisPerBulan($tahun),
        );
        return json_encode($data);
    }
    
    private function _getDetailJualJenisPerBulan($tahun) {
        $this->db->select("COUNT(nodo) AS jml, idbln.bulan");
        $this->db->group_by("idbln.bulan");
        $this->db->order_by("idbln.bulan");
        $this->db->join("(SELECT to_char(tgldo,'MM') AS bulan FROM pzu.vl_jual GROUP BY to_char(tgldo,'MM')) AS idbln","idbln.bulan = to_char(vl_jual.tgldo,'MM')","RIGHT");
        $query = $this->db->get("(SELECT * FROM pzu.vl_jual WHERE tk = 'K' AND (to_char(tgldo,'YYYY') = '".$tahun."')) AS vl_jual");
        $res = array();
        foreach ($query->result_array() as $value) {
            $res[]=array(
                "jml" => $value['jml'],
                "bulan" => (int) $value['bulan'],
            );
        }
        return $res;  
        
    }
    
    public function getJualUnitTahun() {
        $periode_awal = $this->input->post('periode_awal');
        $periode_akhir = $this->input->post('periode_akhir');
        $this->db->select("COUNT(nodo) AS jml, nmtipe");
        $this->db->where("(to_char(tgldo,'YYYY-MM-DD') BETWEEN '".$this->apps->dateConvert($periode_awal)."' AND '".$this->apps->dateConvert($periode_akhir)."')");
        $this->db->group_by("nmtipe");
        $this->db->order_by("COUNT(nodo)","DESC");
        $this->db->limit(10);
        $query = $this->db->get("pzu.vl_jual");
        return json_encode($query->result_array());
    }
    
    public function getDetailJualUnitTahun() {
        $periode_awal = $this->input->post('periode_awal');
        $periode_akhir = $this->input->post('periode_akhir');
        $namaunit = $this->input->post('namaunit');
        $this->db->select("COUNT(nodo) AS jml, idbln.bulan");
        $this->db->group_by("idbln.bulan");
        $this->db->order_by("idbln.bulan");
        $this->db->join("(SELECT to_char(tgldo,'MM') AS bulan FROM pzu.vl_jual GROUP BY to_char(tgldo,'MM')) AS idbln","idbln.bulan = to_char(vl_jual.tgldo,'MM')","RIGHT");
        $query = $this->db->get("(SELECT * FROM pzu.vl_jual WHERE nmtipe = '".$namaunit."' AND (to_char(tgldo,'YYYY-MM-DD') BETWEEN '".$this->apps->dateConvert($periode_awal)."' AND '".$this->apps->dateConvert($periode_akhir)."')) AS vl_jual");
        $res = array();
        foreach ($query->result_array() as $value) {
            $res[]=array(
                "jml" => $value['jml'],
                "bulan" => (int) $value['bulan'],
            );
        }
        return json_encode($res);        
    }
    
    public function getJualHeaderSalesTahun() {
        $periode_awal = $this->input->post('periode_awal');
        $periode_akhir = $this->input->post('periode_akhir');
        $this->db->select("COUNT(nodo) AS jml, nmsales_header");
        $this->db->where("(to_char(tgldo,'YYYY-MM-DD') BETWEEN '".$this->apps->dateConvert($periode_awal)."' AND '".$this->apps->dateConvert($periode_akhir)."')");
        $this->db->group_by("nmsales_header");
        $this->db->order_by("COUNT(nodo)","DESC");
        $this->db->limit(10);
        $query = $this->db->get("pzu.vl_jual");
        return json_encode($query->result_array());
    }
    
    public function getJualSalesTahun() {
        $periode_awal = $this->input->post('periode_awal');
        $periode_akhir = $this->input->post('periode_akhir');
        $nmsales_header = $this->input->post('nmsales_header');
        $this->db->select("COUNT(nodo) AS jml, nmsales");
        $this->db->where("(to_char(tgldo,'YYYY-MM-DD') BETWEEN '".$this->apps->dateConvert($periode_awal)."' AND '".$this->apps->dateConvert($periode_akhir)."')");
        $this->db->where('nmsales_header',$nmsales_header);
        $this->db->group_by("nmsales");
        $this->db->order_by("COUNT(nodo)","DESC");
        $this->db->limit(10);
        $query = $this->db->get("pzu.vl_jual");
        return json_encode($query->result_array());
    }
    
    public function getDetailJualSalesTahun() {
        $periode_awal = $this->input->post('periode_awal');
        $periode_akhir = $this->input->post('periode_akhir');
        $namasales = $this->input->post('namasales');
        $this->db->select("COUNT(nodo) AS jml, idbln.bulan");
        $this->db->group_by("idbln.bulan");
        $this->db->order_by("idbln.bulan");
        $this->db->join("(SELECT to_char(tgldo,'MM') AS bulan FROM pzu.vl_jual GROUP BY to_char(tgldo,'MM')) AS idbln","idbln.bulan = to_char(vl_jual.tgldo,'MM')","RIGHT");
        $query = $this->db->get("(SELECT * FROM pzu.vl_jual WHERE nmsales = '".$namasales."' AND (to_char(tgldo,'YYYY-MM-DD') BETWEEN '".$this->apps->dateConvert($periode_awal)."' AND '".$this->apps->dateConvert($periode_akhir)."')) AS vl_jual");
        $res = array();
        foreach ($query->result_array() as $value) {
            $res[]=array(
                "jml" => $value['jml'],
                "bulan" => (int) $value['bulan'],
            );
        }
        return json_encode($res);        
    }
}
