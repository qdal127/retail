<?php
/*
 * ***************************************************************
 * Script : adh.php
 * Version : 
 * Date : Nov 21, 2017 10:11:14 AM
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description : 
 * ***************************************************************
 */
?>
<style type="text/css">
    .dt-buttons{
        margin-bottom: 10px;
    }
    div.dataTables_processing { z-index: 1; }
</style>
<!-- Main row -->
<div class="row">
    <div class="col-lg-12">
        <div class="box box-danger">
            <div class="box-header">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i> Periode
                                </div>
                                <?= form_input($form['periode_stok']); ?>
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-default btn-tampil">Tampil</button>
                                </div>
                                <!-- /btn-group -->
                            </div>
                            <?= form_error('periode_stok', '<div class="note">', '</div>'); ?>
                        </div>
                    </div>
                </div>                
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-lg-12">
                        <h4><i class="fa fa-line-chart"></i> Rekap Stok Obat <h4>
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered dataTable" style="margin-top: 0px !important;">
                                <thead>
                                    <tr>
                                        <th style="width:10px;text-align: center;">No.</th>
                                        <th style="width: 90px;text-align: center;">Nama Item</th> 
                                        <th style="width: 90px;text-align: center;">Harga</th>
                                        <th style="width: 90px;text-align: center;">Sisa Stock</th>
                                        <th style="width: 90px;text-align: center;">Total</th> 
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th style="width: 90px;text-align: center;">No.</th>
                                        <th style="width: 90px;text-align: right;"></th>
                                        <th style="width: 90px;text-align: right;"></th>
                                        <th style="width: 90px;text-align: right;">Qty</th>
                                        <th style="width: 90px;text-align: right;">Total</th> 
                                    </tr>
                                </tfoot>
                                <tbody></tbody>
                            </table>
                        </div>  
                    </div>   
                </div>
            </div>
        </div>  
    </div> 
</div>  

<script src="http://cdn.datatables.net/plug-ins/1.10.20/api/processing().js"></script> 
<script>
    $(function () {
        var periode = $("#periode_stok").val(); 


        getdata();
        
        $('.btn-tampil').click(function () {
            getdata();
        }); 
    });  

    function getdata(){
        var periode_stok = $("#periode_stok").val();  
        var column = [];

        column.push({
            "aTargets": [ 2,4 ],
            "mRender": function (data, type, full) {
                return type === 'export' ? data : numeral(data).format('0,0.00');
            },
            "sClass": "right"
        });

        column.push({
            "aTargets": [ 3 ],
            "mRender": function (data, type, full) {
                return type === 'export' ? data : numeral(data).format('0,0');
            },
            "sClass": "right"
        });

        table = $('.dataTable').DataTable({
            "aoColumnDefs": column,
            "columns": [
                { "data": "kditem" },
                { "data": "nmitem" }, 
                { "data": "harga" }, 
                { "data": "sak_qty" },
                { "data": "sak_total" }
            ],
            // "lengthMenu": [[ -1], [ "Semua Data"]],
            "lengthMenu": [[10,25,50, 100,500,1000, -1], [10,25,50, 100,500,1000, "Semua Data"]],
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "bAutoWidth": false,
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push({ "name": "periode_stok", "value": periode_stok });
                $.ajax( {
                    "dataType": 'json',
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": fnCallback
                } );
            },
            'rowCallback': function(row, data, index){
                //if(data[23]){
                    //$(row).find('td:eq(23)').css('background-color', '#ff9933');
                //}
            },
            "sAjaxSource": "<?=site_url('dashboard/json_dgview_stok');?>",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu..."
            },
            // jumlah TOTAL
            // 'footerCallback': function ( row, data, start, end, display ) {
            //     var api = this.api(), data;

            //     // converting to interger to find total
            //     var intVal = function ( i ) {
            //         return typeof i === 'string' ?
            //             i.replace(/[\$,]/g, '')*1 :
            //             typeof i === 'number' ?
            //                 i : 0;
            //     };

            //     // computing column Total of the complete result 

            //     var sak_tot = api
            //         .column( 4 )
            //         .data()
            //         .reduce( function (a, b) {
            //             return intVal(a) + intVal(b);
            //         }, 0 );  

            //     // Update footer by showing the total with the reference of the column index
            //     $( api.column( 1 ).footer() ).html('<b>Total</b>'); 
            //     $( api.column( 4 ).footer() ).html(numeral(sak_tot).format('0,0.00'));
            // },
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                {extend: 'copy', footer: true,
                    exportOptions: {orthogonal: 'export' }},
                {extend: 'csv', footer: true,
                    exportOptions: {orthogonal: 'export' }},
                {extend: 'excel', footer: true,
                    exportOptions: {orthogonal: 'export' }},
                {extend: 'pdf', footer: true,
                    orientation: 'landscape',
                    pageSize: 'A0'
                },
                {extend: 'print', footer: true, header: true,
                    customize: function (win){
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '12px');
                        $(win.document.body).find('table')
                                            .addClass('compact')
                                            .css('font-size', 'inherit');
                    }
                }
            ],
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>B r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
        });

        $('.dataTable').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        });

        $('.dataTable tfoot th').each( function () {
            var title = $('.dataTable thead th').eq( $(this).index() ).text();
            if(title!=="Qty" && title!=="Total" && title!=="Delete"){
                $(this).html( '<input type="text" class="form-control input-sm" style="width:100%;border-radius: 0px;" placeholder="Search" />' );
            }else{
                $(this).html( '' );
            }
        } );

        table.columns().every( function () {
            var that = this;
            $( 'input', this.footer() ).on( 'keyup change', function (ev) {
                //if (ev.keyCode == 13) { //only on enter keypress (code 13)
                    that
                        .search( this.value )
                        .draw();
                //}
            } );
        }); 
    }
</script>